<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// $route['default_controller'] = 'MainController/HomepageMethod';
$route['default_controller'] = 'AdminCon/index';
$route['404_override'] = 'MainController/notfound';
$route['translate_uri_dashes'] = FALSE;
$route['common']  ='MainController/commonMethod';
$route['header']  ='MainController/HeaderMethod';
$route['homepage']  ='MainController/HomepageMethod';
$route['signOut'] = 'MainController/signOut';

// ****************************************************************************************************
//AdminPanel Routes Files  - Start
$route['admin/login']  ='AdminCon/index';
$route['admin/common']  ='AdminCon/AdminDashboard';
$route['admin/logout']  ='AdminCon/logout';
$route['admin/dashboard']  ='AdminCon/AdminDashboardMain';
$route['admin/driversList'] = 'AdminCon/driversList';
$route['admin/travellersList'] = 'AdminCon/travellersList';
$route['admin/editUser'] = 'AdminCon/editUser';
$route['admin/paymentDashboard'] = 'AdminCon/paymentDashboard';
//Vehicle and Pricing List
$route['admin/addVehiclePricing'] = 'AdminCon/addVehiclePricing';
$route['admin/editVehiclePricing'] = 'AdminCon/editVehiclePricing';
$route['admin/vehiclePricingList'] = 'AdminCon/vehiclePricingList';
$route['admin/tripList'] = 'AdminCon/tripList';
//AdminPanel Routes Files  - End