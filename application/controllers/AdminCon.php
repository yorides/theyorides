<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admincon extends CI_Controller{
	public function __construct(){
		parent :: __construct();
		$this->load->model(array("Admin_model","Mainmodel"));
		$this->load->helper(array('form', 'url','security')); 
		$this->load->library(array("form_validation",'session','upload','pagination'));
	}
	public function index(){
		if($this->input->post("submit")){
			$this->form_validation->set_rules('username','Username','required|alpha');
			$this->form_validation->set_rules("password",'Password','required|min_length[8]|max_length[8]');
			if($this->form_validation->run() == false){
				$logindata['username'] = $this->input->post("username");
				$logindata['password'] = $this->input->post("password");
				$success = $this->Admin_model->checkUserExists($logindata);
				if($success){
					$this->session->set_userdata("user",$this->input->post("username"));
					$this->session->set_userdata("sess_auth",md5($logindata['username']));
					redirect("admin/dashboard");
				}else{
					$this->session->set_flashdata("ErrMsg","Invalid Login Credentials");
					redirect("admin/login");
				}
			}else{
				redirect("admin/login");
			}
		}
		$this->load->view("admin/login");
	}

	public function AdminDashboard(){
		$this->load->view("admin/common");
	}
	public function AdminDashboardMain(){
		$data['url'] = 'admin/dashboard';
		$user = $this->session->userdata("user");

		$data['driversCount'] = count($this->Mainmodel->getDriverVehiclesRegList()??[]);
		$data['travellerCount'] = count($this->Mainmodel->getTravellersList()??[]);
		$data['otpCount'] = count($this->Mainmodel->getOTPList()??[]);
		$data['vehiclesCount'] = count($this->Mainmodel->getVehiclesList()??[]);
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function vehiclePricingList(){
		$data['url'] = 'admin/vehiclePricingList';
		$data['dbVPData'] = $this->Admin_model->getAllVehicleList();
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function tripList(){
		$data['url'] = 'admin/triplist';
		$data['dbTripData'] = $this->Admin_model->getAllTripsList();
		
		if(!empty($data['dbTripData'])){
			foreach ($data['dbTripData'] as $dbTripKey => $dbTripvalue) {
				$dbTravellerData = $this->Admin_model->getTravellerLatestLoc($dbTripvalue['userId']);
				$dbDriverData = $this->Admin_model->getDriverLatestLoc($dbTripvalue['driverId']);
				$dbVehicleName = $this->Admin_model->getVehicleConfig($dbTripvalue['vehicleId']);
				$data['dbTripData'][$dbTripKey]['travellerName'] = $dbTravellerData[0]['userName'];
				$data['dbTripData'][$dbTripKey]['driverName'] = !empty($dbDriverData[0]['userName'])?$dbDriverData[0]['userName']:"Driver Unregistered";
				$data['dbTripData'][$dbTripKey]['userMobile'] = $dbTravellerData[0]['signInMobile'];
				$data['dbTripData'][$dbTripKey]['vehicleName'] = $dbVehicleName[0]['vType'];
			}
		}

		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function paymentDashboard(){
		$data['url'] = 'admin/paymentDashboard';
		$data['dbVPData'] = $this->Admin_model->getVehicleList();
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function addVehiclePricing(){
		$data['url'] = 'admin/addVehiclePricing';
		if($this->input->post("submit")){
			if(!empty($this->input->post('vehicleName'))){
				$data_to_insert['vType'] = trim($this->input->post('vehicleName'));
				$data_to_insert['pricePerKM'] = ($this->input->post('vehiclePricePerKM'));
				$data_to_insert['surCharge'] = ($this->input->post('vehicleSurcharges'));
				$data_to_insert['SGST'] = ($this->input->post('SGST'));
				$data_to_insert['CGST'] = ($this->input->post('CGST'));
				$data_to_insert['baseFare'] = ($this->input->post('baseFare'));
				$data_to_insert['hourlyCharge'] = ($this->input->post('hourlyCharge'));
				$data_to_insert['isActive'] = "yes";
				$checkVType = $this->Admin_model->checkVTypeInDB($data_to_insert);
				if(!empty($checkVType)){
					$this->session->set_flashdata("Succ",'Entered Vehicle Data Already Exists !');
				}else{
					$vehiclePricingAdded = $this->Admin_model->storeVehiclePricingData($data_to_insert);
					if(!empty($vehiclePricingAdded)){
						$this->session->set_flashdata("Succ",'Vehicle Data Updated! ..');
					}else{
						$this->session->set_flashdata("Succ",'Vehicle Data Not Updated! Please Try Again ..');
					}
				}
			}else{
				$this->session->set_flashdata("Succ",'Please enter valid inputs');
				$this->load->view("admin/common",$data);
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function editVehiclePricing(){
		$data['url'] = 'admin/editVehiclePricing';
		$vehicleId = $this->input->get('vehicleId');
		$data['vehicleData'] = $this->Mainmodel->getVehicleData($vehicleId);
		if($this->input->post("submit")){
			// if(!empty($this->input->post('vehiclePricePerKM'))){
			// $data_to_update['vType'] = trim($this->input->post('vehicleName'));
			$data_to_update['pricePerKM'] = ($this->input->post('vehiclePricePerKM'));
			$data_to_update['surCharge'] = ($this->input->post('vehicleSurcharges'));
			$data_to_update['SGST'] = ($this->input->post('SGST'));
			$data_to_update['CGST'] = ($this->input->post('CGST'));
			$data_to_update['baseFare'] = ($this->input->post('baseFare'));
			$data_to_update['hourlyCharge'] = ($this->input->post('hourlyCharge'));
			$data_to_update['isActive'] = ($this->input->post('isActive'));

				// $checkVType = $this->Admin_model->checkVTypeInDB($data_to_update);
				// if(!empty($checkVType)){
				// 	$this->session->set_flashdata("Succ",'Entered Vehicle Data Already Exists !');
				// }else{
			$vehiclePricingEdited = $this->Mainmodel->editVehiclePricingData($data_to_update,$vehicleId);
			if(!empty($vehiclePricingEdited)){
				$this->session->set_flashdata("Succ",'Vehicle Data Updated! ..');
				redirect('admin/vehiclePricingList');
			}else{
				$this->session->set_flashdata("Succ",'Vehicle Data Not Updated! Please Try Again ..');
			}
				// }
			// }else{
			// 	$this->session->set_flashdata("Succ",'Please enter valid inputs');
			// 	$this->load->view("admin/common",$data);
			// }
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}




	public function driversList(){
		$data['url'] = 'admin/driversList';
		$data['dbUserData']= $this->Mainmodel->getDriverVehiclesRegList();
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}


	public function editUser(){
		$data['url'] = 'admin/editUser';
		$userHashId = $this->input->get('userHashId');
		$data['userData'] = $this->Admin_model->getDriverLatestLoc($userHashId);
		if($this->input->post("submit")){
			$data_to_update['allowSignIn'] = ($this->input->post('allowSignIn'));
			$userStatusUpdated = $this->Admin_model->editUserData($data_to_update,$userHashId);
			if(!empty($userStatusUpdated)){
				$this->session->set_flashdata("Succ",'User Data Updated! ..');
				if($data['userData'][0]['userType']=='D'){
					redirect("admin/driversList");
				}else{
					redirect("admin/travellersList");
				}
			}else{
				$this->session->set_flashdata("Succ",'User Data Not Updated! Please Try Again ..');
			}
		}
		$user = $this->session->userdata("user");
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function travellersList(){
		$data['url'] = 'admin/travellersList';
		$data['dbTravellerData'] = $this->Mainmodel->getTravellersList();
		if(!empty($this->session->userdata("sess_auth"))){
			$this->load->view("admin/common",$data);
		}else{
			session_unset();
			redirect("admin/login");
		}
	}

	public function notfound(){
		$this->load->view("admin/notfound");
	}

	public function email(){
		$this->load->view('admin/email');
	}

	public function logout(){
		$this->load->view('admin/logout');
	}

}
?>