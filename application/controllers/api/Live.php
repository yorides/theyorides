		<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
		require APPPATH . 'libraries/REST_Controller.php';
		class Live extends REST_Controller {
			function __construct()
			{
				parent::__construct();
				$this->load->database(); 
				// $this->load->library('upload');
				$this->load->model('Admin_model');
				$this->load->library(array("form_validation",'session','upload','pagination'));
				$this->load->helper(array('form', 'url','security')); 
				define('pricePerKM',10);
				//Encrypt and Decrypt Data
				define('ENCRYPTION_KEY', '__^%&Q@#$%^&*^__');
				define( 'API_ACCESS_KEY', 'AAAA0zmDieA:APA91bHTrc8R24Pb4mhgkZl7-uf_bQNKsMFvFIaSBedImyNNpDAwepunE62n5nRkttTKjDym8ErCJqcQ3NT4SCre8z1Dk4Fy38J8Y370ux3NWQPL5OzX8A62uDWLX6diM1N2clGRNHRW' );
			}
			function uploadImage($imageName,$file){
				$target_path = "./uploads/".$file."/";
				$target_path = $target_path .$imageName.".png"; 
				if(move_uploaded_file($_FILES[$file]['tmp_name'], $target_path)) {
					return 1;
				} else{
					return null;
				}
			}
			// ********************************************************************************************************
			public function CalDis($lat1, $lon1, $lat2, $lon2, $unit) {
				$theta = $lon1 - $lon2;
				$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
				$dist = acos($dist);
				$dist = rad2deg($dist);
				$miles = $dist * 60 * 1.1515;
				$unit = strtoupper($unit);
				if ($unit == "K") {
					return ($miles * 1.609344);
				} else if ($unit == "N") {
					return ($miles * 0.8684);
				} else {
					return $miles;
				}
			}
		// ********************************************************************************
			public function sendFCM($msgTitle,$fcmIds,$params,$eventType) {
				$url = "https://fcm.googleapis.com/fcm/send";
				$body = "Yo Rides";
				if(!empty($params)){
					$postParams = "?".http_build_query($params);
				}else {
					$postParams = null;
				}
				$data = array(
					'title' => 'Yo Rides',
					'body' => $msgTitle,
					'sound' => 'default',
					'badge' => '1',
					'titleColor' => '#EC7381',
					"contentBgColor"=> "#FFFFFF",
					"isContent"=> "true",
					"bodyColor"=>"#000000",
					"pushType"=> "1",
					"customParams" =>array(
						"eventType"=> $eventType,
						"bundle"=> $postParams
					)
				);
				// $notification = array(
				// 	'title' =>$msgTitle ,
				// 	'body' => $body,
				// 	'sound' => 'res_notif_sound',
				// 	'badge' => '1',
				// 	'titleColor' => '#EC7381',
				// 	"contentBgColor"=> "#FFFFFF",
				// 	"isContent"=> "true",
				// 	"bodyColor"=>"#000000",
				// 	"pushType"=> "1"
				// );
				$arrayToSend = array(
					'to' => $fcmIds,
					// 'notification' => $notification,
					'priority'=> 'high',
					'data'=>$data
				);
				$json = json_encode($arrayToSend);
				$headers = array();
				$headers[] = 'Content-Type: application/json';
				$headers[] = 'Authorization: key='.API_ACCESS_KEY;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		    //Send the request
				$response = curl_exec($ch);
		    //Close request
				if ($response === FALSE) {
					die('FCM Send Error: ' . curl_error($ch));
				}
				curl_close($ch);
				return true;
			}
			// *************************************************************************************
			// CONFIRM OTP API
			public function confirmOtp_post(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$userType = (!empty($inputArray['userType'])?$inputArray['userType']:$this->response([
					'responseMsg' => "userType is required (Either T or D)",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$otpReceived = (!empty($inputArray['otpReceived'])?$inputArray['otpReceived']:$this->response([
					'responseMsg' => "otpReceived is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$signInMobile = (!empty($inputArray['signInMobile'])?$inputArray['signInMobile']:$this->response([
					'responseMsg' => "signInMobile No is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$confirmOTPinDB=$this->Admin_model->checkOtpData($otpReceived,$signInMobile,$userType);
				if(!empty($confirmOTPinDB)){
					if(time()-($confirmOTPinDB[0]['otpGenTime']) >= 30) {
						$this->response([
							'responseMsg' => "OTP is expired",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}else{
						$userData['userType'] = (!empty($userType)?$userType:"");
						$userData['signInMobile'] = (!empty($signInMobile)?$signInMobile:"");
						$userData['allowSignIn'] = 1;
						$userDetails['signInMobile'] = $signInMobile;
						$checkUserExist = $this->Admin_model->checkUserData($userDetails);
						if(!empty($checkUserExist)){
							$signInMobile = $userDetails['signInMobile'];
							$detailsData['allowSignIn'] = 1;
							$updateDeviceDetails = $this->Admin_model->updateUserDetails($detailsData,$signInMobile);
							$this->response([
								'responseMsg' => "user already exist",
								'responseCode' => 200,
								'responseData' => array(
									"isUserExists"=>true,
									"userId" => $checkUserExist[0]['userHashId'],
									"userType" => $checkUserExist[0]['userType']
								),
							], REST_Controller::HTTP_BAD_REQUEST);
						}else{
							$dbUserId = $this->Admin_model->storeUserData($userData);
							if(!empty($dbUserId)){
								$updateUserHash = $this->Admin_model->updateHashId($dbUserId);
								$this->response([
									'responseMsg' => "OTP is verified",
									'responseCode' => 200,
									'responseData' => array('userType'=>$userType,"userId" =>md5($dbUserId)),
								], REST_Controller::HTTP_OK);
							}else{
								$this->response([
									'responseMsg' => "OTP is invalid",
									'responseCode' => 400,
								], REST_Controller::HTTP_BAD_REQUEST);
							}
						}
					}
				}else{
					$this->response([
						'responseMsg' => "OTP is invalid",
						'responseCode' => 400,
					], REST_Controller::HTTP_BAD_REQUEST);
				}
			}
		// *******************************************************************************************************
			public function sendOtp_post(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$userType = (!empty($inputArray['userType'])?$inputArray['userType']:$this->response([
					'responseMsg' => "userType is required (Either T or D)",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$userTypeArr = array("T","D");
				$countryCode = (!empty($inputArray['countryCode'])?$inputArray['countryCode']:$this->response([
					'responseMsg' => "countryCode is required (eg +91)",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$signInMobile = (!empty($inputArray['signInMobile'])?$inputArray['signInMobile']:$this->response([
					'responseMsg' => "signInMobile No is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$userDetails['signInMobile'] = $signInMobile;
				// $userDetails['allowSignIn'] = 1;
				// $userDetails['userType'] = $userType;
				$checkUserExist = $this->Admin_model->checkUserData($userDetails);
				if(!empty($checkUserExist)){
					if(($checkUserExist[0]['allowSignIn']==1)and($checkUserExist[0]['userType']==$userType)){
						$otp = substr(str_shuffle("0123456789"),0, 4);
						$otpGenTime = time();
						$dataArr = array("userType"=> $userType,"countryCode"=>$countryCode,"mobile"=>$signInMobile,"otp"=>$otp,"otpGenTime"=>$otpGenTime,"hash"=>md5($signInMobile));
						$StoreToDb = $this->Admin_model->storeSendOtpData($dataArr);
						$apiUrl = "http://m.messagewall.in/api/v2/sms/send?access_token=f9ea273b45ff6a8e156b935b07605aca&to=$signInMobile&message=Yo Rides OTP:$otp&service=T&sender=AATEXT";
						$sendOtp = file_get_contents($apiUrl);
						$sendOtp = true;
						if(!empty($sendOtp)){
							$this->response([
								"responseMsg" => "OTP has been sent",
								"responseCode" => 200,
								"otpResponseData" => array("otp"=> $otp)
							], REST_Controller::HTTP_OK);
						}else{
							$this->response([
								'responseMsg' => "OTP sending is failure",
								'responseCode' => 400,
							], REST_Controller::HTTP_BAD_REQUEST);
						}
					}elseif(($checkUserExist[0]['allowSignIn']==0)and($checkUserExist[0]['userType']==$userType)){
						$otp = substr(str_shuffle("0123456789"),0, 4);
						$otpGenTime = time();
						$dataArr = array("userType"=> $userType,"countryCode"=>$countryCode,"mobile"=>$signInMobile,"otp"=>$otp,"otpGenTime"=>$otpGenTime,"hash"=>md5($signInMobile));
						$StoreToDb = $this->Admin_model->storeSendOtpData($dataArr);
						$apiUrl = "http://m.messagewall.in/api/v2/sms/send?access_token=f9ea273b45ff6a8e156b935b07605aca&to=$signInMobile&message=Yo Rides OTP:$otp&service=T&sender=AATEXT";
						// 
						$sendOtp = file_get_contents($apiUrl);
						
				// $sendOtp = true;
						if(!empty($sendOtp)){
							$this->response([
								"responseMsg" => "OTP has been sent",
								"responseCode" => 200,
								"otpResponseData" => array("otp"=> $otp)
							], REST_Controller::HTTP_OK);
						}else{
							$this->response([
								'responseMsg' => "OTP sending is failure",
								'responseCode' => 400,
							], REST_Controller::HTTP_BAD_REQUEST);
						}
					}else{
						$userModeFromDb = ($checkUserExist[0]['userType']=="D")?"driver":"traveller";
						$mess = "The same number is already used in ".$userModeFromDb;
						$this->response([
							'responseMsg' => $mess,
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}else{
					$otp = substr(str_shuffle("0123456789"),0, 4);
					$otpGenTime = time();
					$dataArr = array("userType"=> $userType,"countryCode"=>$countryCode,"mobile"=>$signInMobile,"otp"=>$otp,"otpGenTime"=>$otpGenTime,"hash"=>md5($signInMobile));
					$StoreToDb = $this->Admin_model->storeSendOtpData($dataArr);
					$apiUrl = "http://m.messagewall.in/api/v2/sms/send?access_token=f9ea273b45ff6a8e156b935b07605aca&to=$signInMobile&message=Yo Rides OTP:$otp&service=T&sender=AATEXT";
					$sendOtp = file_get_contents($apiUrl);
				// $sendOtp = true;
					if(!empty($sendOtp)){
						$this->response([
							"responseMsg" => "OTP has been sent",
							"responseCode" => 200,
							"otpResponseData" => array("otp"=> $otp)
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseMsg' => "OTP sending is failure",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}
			}
		// *********************************************************************************************************************
			public function vehicleReg_post(){

				$userId = (!empty($_POST['userId'])?$_POST['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$signInMobile = (!empty($_POST['signInMobile'])?$_POST['signInMobile']:$this->response([
					'responseMsg' => "signInMobile No is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$vehicleRegNo = (!empty($_POST['vehicleRegNo'])?$_POST['vehicleRegNo']:$this->response([
					'responseMsg' => "vehicleRegNo is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$licNo = (!empty($_POST['licNo'])?$_POST['licNo']:$this->response([
					'responseMsg' => "licNo is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$licCopy = (!empty($_FILES['licCopy'])?$_FILES['licCopy']:$this->response([
					'responseMsg' => "licCopy is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$tradeLicCopy = (!empty($_FILES['tradeLicCopy'])?$_FILES['tradeLicCopy']:$this->response([
					'responseMsg' => "tradeLicCopy is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$routePermitCopy = (!empty($_FILES['routePermitCopy'])?$_FILES['routePermitCopy']:$this->response([
					'responseMsg' => "routePermitCopy is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$vehiclePhotoFront = (!empty($_FILES['vehiclePhotoFront'])?$_FILES['vehiclePhotoFront']:$this->response([
					'responseMsg' => "vehiclePhotoFront is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$vehiclePhotoBack = (!empty($_FILES['vehiclePhotoBack'])?$_FILES['vehiclePhotoBack']:$this->response([
					'responseMsg' => "vehiclePhotoBack is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$vehicleId = (!empty($_POST['vehicleId'])?$_POST['vehicleId']:$this->response([
					'responseMsg' => "vehicleId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$regCopyFront = (!empty($_FILES['regCopyFront'])?$_FILES['regCopyFront']:$this->response([
					'responseMsg' => "regCopyFront is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$regCopyBack = (!empty($_FILES['regCopyBack'])?$_FILES['regCopyBack']:$this->response([
					'responseMsg' => "regCopyBack is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$userDetails['signInMobile'] = $signInMobile;
				$userDetails['userHashId'] = $userId;
				$checkUserExist = $this->Admin_model->checkUserData($userDetails);

				if(!empty($checkUserExist)){

					$userMode = $checkUserExist[0]['userType'];
					if($userMode=='D'){


						$userData["signInMobile"] =  $_POST['signInMobile'];
						$userData["userHashId"] =  ($_POST['userId']);
						$userData["vehicleRegNo"] =  $_POST['vehicleRegNo'];
						$userData["licNo"] =  $_POST['licNo'];
						$userData["vehicleId"] =  $_POST['vehicleId'];

					//regCopyFront
						$imageName = $_POST['userId'];
						$file = 'regCopyFront';
						$regCopyFrontUpload = $this->uploadImage($imageName,$file);

					//regCopyBack
						$imageName = $_POST['userId'];
						$file = 'regCopyBack';
						$regCopyBackUpload = $this->uploadImage($imageName,$file);

					//lic Copy
						$imageName = $_POST['userId'];
						$file = 'licCopy';
						$licCopyUpload = $this->uploadImage($imageName,$file);

					//tradeLicCopy
						$imageName = $_POST['userId'];
						$file = 'tradeLicCopy';
						$tradeLicUpload = $this->uploadImage($imageName,$file);

					//routePermitCopy
						$imageName = $_POST['userId'];
						$file = 'routePermitCopy';
						$routePermitUpload = $this->uploadImage($imageName,$file);

					//vehiclePhotoFront
						$imageName = $_POST['userId'];
						$file = 'vehiclePhotoFront';
						$vehiclePhotoFrontUpload = $this->uploadImage($imageName,$file);

					//vehiclePhotoBack
						$imageName = $_POST['userId'];
						$file = 'vehiclePhotoBack';
						$vehiclePhotoBackUpload = $this->uploadImage($imageName,$file);

						$vData['signInMobile'] = $userData["signInMobile"];
						$checkVehicleData = $this->Admin_model->checkVehicleData($vData);

						$vehicleTypeIs = $this->Admin_model->getVehicleConfig($vehicleId);

						if(!empty($checkVehicleData)){
							$this->response([
								'responseCode' => 400,
								'responseMsg' => "Vehicle Reg data is already updated",
							// 'responseData' => array('vehicleid'=>$vehicleId),
							], REST_Controller::HTTP_BAD_REQUEST);
						}else{
							$storeVehicleData = $this->Admin_model->storeVehicleData($userData);
							if(!empty($storeVehicleData)){

								$storedData['userId'] = $userId;
								$storedData['signInMobile'] = $signInMobile;
								$storedData['vehicleRegNo'] = $vehicleRegNo;
								$storedData['licNo'] = $licNo;
								$storedData['vehicleId'] = $vehicleId;
								$storedData['vehicleType'] = $vehicleTypeIs[0]['vType'] ?? null;
								$storedData['licCopy'] = base_url().'uploads/licCopy/'.$userId.'.png';
								$storedData['tradeLicCopy'] = base_url().'uploads/tradeLicCopy/'.$userId.'.png';
								$storedData['routePermitCopy'] = base_url().'uploads/routePermitCopy/'.$userId.'.png';
								$storedData['vehiclePhotoFront'] = base_url().'uploads/vehiclePhotoFront/'.$userId.'.png';
								$storedData['vehiclePhotoBack'] = base_url().'uploads/vehiclePhotoBack/'.$userId.'.png';
								$storedData['regCopyFront'] = base_url().'uploads/regCopyFront/'.$userId.'.png';
								$storedData['regCopyBack'] = base_url().'uploads/regCopyBack/'.$userId.'.png';

								$this->response([
									'responseMsg' => "Vehicle Reg data is updated",
									'responseCode' => 200,
									'responseData' => array($storedData)
								], REST_Controller::HTTP_OK);
							}else{
								$this->response([
									'responseMsg' => "Vehicle Reg updating is failed",
									'responseCode' => 400,
								], REST_Controller::HTTP_BAD_REQUEST);
							}
						}
					}else{
						$this->response([
							'responseMsg' => "The same number is already used in traveller",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}

				}else{
					$this->response([
						'responseMsg' => "signInMobile is invalid or Mobile no is incorrect",
						'responseCode' => 400,
					], REST_Controller::HTTP_BAD_REQUEST);
				}
			}


		// *******************************************************************************************************
			public function paymentModeList_GET(){
				$getPaymentModeList = $this->Admin_model->getPaymentModeList();
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "Payment Mode List Details",
					'responseData' => $getPaymentModeList,
				], REST_Controller::HTTP_OK);
			}

			// *********************************************************************************************************************
			public function vehicleList_GET(){
				$getVehicleList = $this->Admin_model->getVehicleList();
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "Vehicle List Details",
					'responseData' => $getVehicleList,
				], REST_Controller::HTTP_OK);
			}
		// *******************************************************************************************************
				// User Existence
			public function fetchUserInfo_post(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$userType = (!empty($inputArray['userType'])?$inputArray['userType']:$this->response([
					'responseMsg' => "userType is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$deviceID = (!empty($inputArray['deviceID'])?$inputArray['deviceID']:$this->response([
					'responseMsg' => "deviceID is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$deviceIp = (!empty($inputArray['deviceIp'])?$inputArray['deviceIp']:$this->response([
					'responseMsg' => "deviceIp is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$deviceMac = (!empty($inputArray['deviceMac'])?$inputArray['deviceMac']:$this->response([
					'responseMsg' => "deviceMac is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$deviceOs = (!empty($inputArray['deviceOs'])?$inputArray['deviceOs']:$this->response([
					'responseMsg' => "deviceOs is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$fcmToken = (!empty($inputArray['fcmToken'])?$inputArray['fcmToken']:$this->response([
					'responseMsg' => "fcmToken is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$userData['userType'] = $userType;
				$userData['userHashId'] = $userId;
				$userData['allowSignIn'] = 1;
				$checkUserExist = $this->Admin_model->checkUserExist($userData);
				if(!empty($checkUserExist)){
					$deviceData['deviceId'] = $deviceID;
					$deviceData['deviceIp'] = $deviceIp;
					$deviceData['deviceMac'] = $deviceMac;
					$deviceData['deviceOs'] = $deviceOs;
					$deviceData['fcmToken'] = $fcmToken;
					$deviceData['allowSignIn'] = 1;
					$updateDeviceDetails = $this->Admin_model->updateDeviceDetails($deviceData,$userId);
					if(!empty($updateDeviceDetails)){
						$this->response([
							'responseMsg' => "user exist",
							"responseData"=> array(
								"signInMobile"=>$checkUserExist[0]['signInMobile'],
								"userEmail"=>$checkUserExist[0]['userEmail'],
								"userName"=>$checkUserExist[0]['userName'],
								"userImage"=> base_url().'/uploads/userImage/'.$checkUserExist[0]['userHashId'].'.png',
								"deviceID"=>$checkUserExist[0]['deviceID'],
								"deviceMac"=>$checkUserExist[0]['deviceMac'],
								"deviceIp"=>$checkUserExist[0]['deviceIp'],
								"deviceOs"=>$checkUserExist[0]['deviceOs'],
								"fcmToken"=>$checkUserExist[0]['fcmToken']),
							'responseCode' => 200,
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseMsg' => "user not exist",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}else{
					$this->response([
						'responseMsg' => "user is signed out",
						'responseCode' => 400,
					], REST_Controller::HTTP_BAD_REQUEST);
				}
			}

		// *************************************************************************************

			public function cutomerSupport_get(){
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "Customer Support Details",
					'responseData' => array("mobile"=>"9748703099"),
				], REST_Controller::HTTP_OK);

			}

		// *************************************************************************************

			public function updateProfileInfo_post(){

				$userId = (!empty($_POST['userId'])?$_POST['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$userName = (!empty($_POST['userName'])?$_POST['userName']:$this->response([
					'responseMsg' => "userName is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$userEmail = (!empty($_POST['userEmail'])?$_POST['userEmail']:$this->response([
					'responseMsg' => "userEmail is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				// $refferalCode = $_POST['refferalCode'];
				// $userImage = $_FILES['userImage'];
				if(!empty($_FILES['userImage'])){
					$imageName = $userId;
					$file = 'userImage';
					$profileImageUpload = $this->uploadImage($imageName,$file);
				}
				$userProfile['userEmail'] = $userEmail;
				$userProfile['userName'] = $userName;
				if(!empty($_POST['refferalCode'])){
					$userProfile['refferalCode'] = $refferalCode = $_POST['refferalCode'];
				}
				$userData['userHashId'] = $userId;
				$checkUserExist = $this->Admin_model->checkUserData($userData);
				if(!empty($checkUserExist)){
					$addProfileData = $this->Admin_model->addProfileData($userProfile,$userId);
					if(!empty($addProfileData)){
						$this->response([
							'responseMsg' => "User Profile Updated",
							'responseCode' => 200,
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseMsg' => "User Profile Not Updated",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}else{
					$this->response([
						'responseMsg' => "User Not Found",
						'responseCode' => 400,
					], REST_Controller::HTTP_OK);
				}

			}

		// ***************************************************************************************************

			public function updateLocation_post(){

				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$latitude = (!empty($inputArray['latitude'])?$inputArray['latitude']:$this->response([
					'responseMsg' => "latitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$longitude = (!empty($inputArray['longitude'])?$inputArray['longitude']:$this->response([
					'responseMsg' => "longitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				if(!empty($inputArray['userAddress'])){
					$userAddress = $inputArray['userAddress'];
					$userLocationData['userAddress'] = $userAddress;
				}
				$userLocationData['latitude'] = $latitude;
				$userLocationData['longitude'] = $longitude;
				$userData['userHashId'] = $userId;
				$checkUserExist = $this->Admin_model->checkUserData($userData);
				if(!empty($checkUserExist)){
					$locationUpdated = $this->Admin_model->updateUserLocation($userLocationData,$userId);
					if(!empty($locationUpdated)){
						$this->response([
							'responseMsg' => "User location Updated",
							'responseCode' => 200,
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseMsg' => "User location Not Updated",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}else{
					$this->response([
						'responseMsg' => "User Not Found",
						'responseCode' => 400,
					], REST_Controller::HTTP_OK);
				}

			}

		// *********************************************************************************************************

			public function travellerLookingForTrip_POST(){

				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$userId = $travellerId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$fromLatitude = (!empty($inputArray['fromLatitude'])?$inputArray['fromLatitude']:$this->response([
					'responseMsg' => "fromLatitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$fromLongitude = (!empty($inputArray['fromLongitude'])?$inputArray['fromLongitude']:$this->response([
					'responseMsg' => "fromLongitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$toLatitude = (!empty($inputArray['toLatitude'])?$inputArray['toLatitude']:$this->response([
					'responseMsg' => "toLatitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$toLongitude = (!empty($inputArray['toLongitude'])?$inputArray['toLongitude']:$this->response([
					'responseMsg' => "toLongitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				if(!empty($inputArray['fromAddress'])){
					$fromAddress = $inputArray['fromAddress'];
				}else{
					$fromAddress = "";
				}
				if(!empty($inputArray['toAddress'])){
					$toAddress = $inputArray['toAddress'];
				}else{
					$toAddress = "";
				}
				$userData['userHashId'] = $userId;
				$checkUserExist = $this->Admin_model->checkUserData($userData);
				if(!empty($checkUserExist)){
					$distInKM= $this->CalDis($fromLatitude,$fromLongitude,$toLatitude,$toLongitude,"K");
					if($distInKM>0){
						$distInKM = number_format($distInKM,'2');
						$getActiveDriversList = $this->Admin_model->getActiveDriversList();
						$availableDriversList = (array_column($getActiveDriversList, "vehicleId"));
						if(!empty($availableDriversList)){
							$getFilteredVehicleList = $this->Admin_model->getFilteredVehicleList($availableDriversList);
							foreach ($getFilteredVehicleList as $key =>$value) {
								$vehicles[$key]['estimatedAmount']	= (int)($value['pricePerKM']*$distInKM*$value['surCharge']);	
								$vehicles[$key]['vType']	= ucwords(strtolower($value['vType']));				
								$vehicles[$key]['vehicleId']	= $value['vehicleId'];				
								$vehicles[$key]['distInKM']	= $distInKM;				
								$vehicles[$key]['surCharge']	= (int)$value['surCharge'];
								$vehicles[$key]['pricePerKM']	= (int)$value['pricePerKM'];	
							}
							sort($vehicles);
							$defaultPaymentTypeData = $this->Admin_model->checkPreviousTripPaymentType($travellerId);
							if(!empty($defaultPaymentTypeData)){
								$pmId = $defaultPaymentTypeData[0]['pmId'];
								$paymentModeDetail = $this->Admin_model->getPaymentModeDetails($pmId);
								$paymentMode = $paymentModeDetail[0]['paymentMode'];
								$defaultPaymentType = array("pmId"=>$pmId,"paymentMode"=>$paymentMode);
							}
							// else{
								// $defaultPaymentType = array("pmId"=>"noTripCompletedYet","paymentMode"=>"noTripCompletedYet");
							// }
							if(isset($defaultPaymentType)){
								$respArr = array(
									"userId" => $checkUserExist[0]['userHashId'],
									"fromLatitude" => $fromLatitude,
									"fromLongitude" => $fromLongitude,
									"toLatitude" => $toLatitude,
									"toLongitude" => $toLongitude,
									"toAddress" => $toAddress,
									"fromAddress" => $fromAddress,
									"defaultPayment" => $defaultPaymentType,
									"estimatedDetails" => $vehicles,
								);
							}else{
								$respArr = array(
									"userId" => $checkUserExist[0]['userHashId'],
									"fromLatitude" => $fromLatitude,
									"fromLongitude" => $fromLongitude,
									"toLatitude" => $toLatitude,
									"toLongitude" => $toLongitude,
									"toAddress" => $toAddress,
									"fromAddress" => $fromAddress,
									"estimatedDetails" => $vehicles,
								);
							}
							$this->response([
								'responseCode' => 200,
								'responseMsg' => "Trip details for Confirmation",
								"responseData" => $respArr
							], REST_Controller::HTTP_OK);
						}
					}else{
						$this->response([
							'responseMsg' => "From location and To location should not be same",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}else{
					$this->response([
						'responseMsg' => "User Not Found",
						'responseCode' => 400,
					], REST_Controller::HTTP_OK);
				}

			}

		// ************************************************************************************************************

			public function travellerConfirmTrip_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$vehicleId = (!empty($inputArray['vehicleId'])?$inputArray['vehicleId']:$this->response([
					'responseMsg' => "vehicleId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$fromLatitude = (!empty($inputArray['fromLatitude'])?$inputArray['fromLatitude']:$this->response([
					'responseMsg' => "fromLatitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$fromLongitude = (!empty($inputArray['fromLongitude'])?$inputArray['fromLongitude']:$this->response([
					'responseMsg' => "fromLongitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$toLatitude = (!empty($inputArray['toLatitude'])?$inputArray['toLatitude']:$this->response([
					'responseMsg' => "toLatitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$toLongitude = (!empty($inputArray['toLongitude'])?$inputArray['toLongitude']:$this->response([
					'responseMsg' => "toLongitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$pmId = (!empty($inputArray['pmId'])?$inputArray['pmId']:$this->response([
					'responseMsg' => "pmId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				if(!empty($inputArray['fromAddress'])){
					$fromAddress = $inputArray['fromAddress'];
				}else{
					$fromAddress = "";
				}
				if(!empty($inputArray['toAddress'])){
					$toAddress = $inputArray['toAddress'];
				}else{
					$toAddress = "";
				}
				$userData['userHashId'] = $userId;
				$checkUserExist = $this->Admin_model->checkUserData($userData);
				if(!empty($checkUserExist)){
					$distInKM= $this->CalDis($fromLatitude,$fromLongitude,$toLatitude,$toLongitude,"K");
					if($distInKM>0){
						$getVehicleList = $this->Admin_model->getVehicleList();
						foreach ($getVehicleList as $key =>$value) {
							$vehicles[$key]['vType']	= $value['vType'];				
							$vehicles[$key]['vehicleId']	= $value['vehicleId'];				
							$vehicles[$key]['distInKM']	= number_format($distInKM,'2');				
							$vehicles[$key]['surCharge']	= (int)$value['surCharge'];
							$vehicles[$key]['pricePerKM']	= (int)$value['pricePerKM'];	
							$vehicles[$key]['estimatedAmount']=number_format($value['pricePerKM']*$distInKM*$value['surCharge']);	
						}
						$selectedVehicleDriverList = $this->Admin_model->selectedVehicleDriverList($vehicleId); 
						if(!empty($selectedVehicleDriverList)){
							foreach ($selectedVehicleDriverList as $kk  => $vv) {
								if(!empty($vv['vehicleRegNo'])&&(!empty($vv['licNo']))){
									$listOfDriversId[] = trim($vv['userHashId']);
								}
							}
							if(!empty($listOfDriversId)){
								// echo '<pre>';
								// print_r($listOfDriversId);
								// echo '</pre>';
								// foreach ($listOfDriversId as $eachDriverKey => $eachDriverIdValue) {
								// 	// echo '<pre>';
								// 	// print_r($eachDriverIdValue);
								// 	// echo '</pre>';
								// 	$currentlyAvailableDrivers = $this->Admin_model->getCurrentlyAvailbleDrivers($eachDriverIdValue);
								// 	echo '<pre>';
								// 	print_r($currentlyAvailableDrivers);
								// 	echo '</pre>';
								// 	break;
								// }
								// die;
								// echo '<pre>';
								// print_r($currentlyAvailableDrivers);
								// echo '</pre>';
								// die;
								$getDriversList = $this->Admin_model->getDriversListUsingUserId($listOfDriversId);
							}else{
								$getDriversList = array();
							}
							if(!empty($getDriversList)){
								$travellerTripReqData['userId'] = $userId;
								$travellerTripReqData['vehicleId'] = $vehicleId;
								$travellerTripReqData['fromLatitude'] = $fromLatitude;
								$travellerTripReqData['fromLongitude'] = $fromLongitude;
								$travellerTripReqData['toLatitude'] = $toLatitude;
								$travellerTripReqData['toLongitude'] = $toLongitude;
								$travellerTripReqData['fromAddress'] = $fromAddress;
								$travellerTripReqData['toAddress'] = $toAddress;
								$travellerTripReqData['tripReqTime'] = time();
								$travellerTripReqData['pmId'] = $pmId;
								$travellerTripReqData['tripStatus'] = 'requested';
								$tripReqId = $this->Admin_model->storeTravellerReqData($travellerTripReqData);
								if(!empty($tripReqId)){
									$tripReqHashId = $this->Admin_model->updateTripReqIdAsHashId($tripReqId);
								}
								//FCM - start
								$getDeviceIds = array_column($this->Admin_model->getFCMIds($listOfDriversId), 'fcmToken');

								if(!empty($getDeviceIds)){
									foreach ($getDeviceIds as $dKey => $fcmIds) {
										$msgTitle = "A Traveller is requesting for Trip !";
										$params = array('tripReqId'=> $tripReqHashId,"travellerId"=>$userId);
										$eventType = "confirmTrip"; //Used for driver when he confirms trips
										$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
									}
								}
								//FCM - end
								$this->response([
									'responseCode' => 200,
									'responseMsg' => "Trip request sent to drivers",
									'responseData' => array('tripReqId' =>  $tripReqHashId),
								], REST_Controller::HTTP_OK);
							}else{
								$this->response([
									'responseMsg' => "No vehicle found ! Try Again",
									'responseCode' => 400,
								], REST_Controller::HTTP_BAD_REQUEST);
							}
						}else{
							$this->response([
								'responseMsg' => "No vehicle found ! Try Again",
								'responseCode' => 400,
							], REST_Controller::HTTP_BAD_REQUEST);
						}
					}else{
						$this->response([
							'responseMsg' => "From location and To location should not be same",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}else{
					$this->response([
						'responseMsg' => "User Not Found",
						'responseCode' => 400,
					], REST_Controller::HTTP_OK);
				}
			}

		// *****************************************************************************************************************
				//requested
				//accepted
				//started
				//cancelled
				//completed
				//rejected
			public function acceptTrip_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
					'responseMsg' => "travellerId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
					'responseMsg' => "driverId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$tripReqId = (!empty($inputArray['tripReqId'])?$inputArray['tripReqId']:$this->response([
					'responseMsg' => "tripReqId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$tripOtp = substr(str_shuffle("0123456789"),0, 4);
				$tripContent['driverId'] = $driverId;
				$tripContent['tripOtp'] = $tripOtp;
				$tripContent['tripStatus'] = 'accepted';
				$tripContent['tripId'] = $tripId = md5($tripReqId);
				$userDetails['userHashId'] = $travellerId;
				$signInMobile = $this->Admin_model->checkUserData($userDetails)[0]['signInMobile'];
				// $apiUrl = "http://m.messagewall.in/api/v2/sms/send?access_token=f9ea273b45ff6a8e156b935b07605aca&to=$signInMobile&message=Yo Rides Trip OTP:$tripOtp&service=T&sender=AATEXT";
				// $sendOtp = file_get_contents($apiUrl);
				$sendOtp = true;
				$tripIsReqOrNot = $this->Admin_model->checkTripAcceptedOrNot($travellerId,$tripReqId);
				if(!empty($tripIsReqOrNot)){
					$tripIsCreated = $this->Admin_model->createTrip($tripContent,$tripReqId);
					if(!empty($tripIsCreated)){
						$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($driverId);
						$vehicleName = $this->Admin_model->getVehicleDetailsUsingDriverId($driverId);
						$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);
						if(empty($driverLatestLoc)||empty($travellerLatestLoc)){
							$this->response([
								'responseCode' => 400,
								'responseMsg' => "trip cant be accepted - invalid driverId / travellerId passed",
							], REST_Controller::HTTP_OK);
						}else{
							$driverImage =  base_url().'uploads/userImage/'.$driverLatestLoc[0]['userHashId'].'.png';
							$travellerImage =  base_url().'uploads/userImage/'.$travellerLatestLoc[0]['userHashId'].'.png';

							$journeyDetails = $this->Admin_model->getJourneyDetailsUsingTripReqId($tripReqId);
							if(!empty($journeyDetails)){
								$vehicleId = $journeyDetails[0]['vehicleId']??null;
								$getvehicleTypeIs = $this->Admin_model->getVehicleConfig($vehicleId);
							}
							$vData['userHashId'] = $driverId;
							$checkVehicleData = $this->Admin_model->checkVehicleData($vData);
							$TripDetails = array(
								"tripId" => $tripId,
								"tripOTP"=> $tripOtp,
								"driverDetails"=> array(
									"driverName"=> $driverLatestLoc[0]['userName']?? null,
									"driverEmail"=> $driverLatestLoc[0]['userEmail']?? null,
									"driverImage"=> $driverImage,
									"driverMobileNo"=> $driverLatestLoc[0]['signInMobile']?? null,
									"driverId"=> $driverLatestLoc[0]['userHashId']?? null,
									"driverCurLat"=> $driverLatestLoc[0]['latitude']?? null,
									"driverCurLong"=> $driverLatestLoc[0]['longitude']?? null,
									"driverCurAddress"=> $driverLatestLoc[0]['userAddress']?? null
								),
								"travellerDetails"=> array(
									"travellerName"=> $travellerLatestLoc[0]['userName']??null,
									"travellerEmail"=> $travellerLatestLoc[0]['userEmail']??null,
									"travellerImage"=> $travellerImage,
									"travellerMobileNo"=> $travellerLatestLoc[0]['signInMobile']??null,
									"travellerId"=> $travellerLatestLoc[0]['userHashId']??null,
									"travellerCurLat"=> $travellerLatestLoc[0]['latitude']??null,
									"travellerCurLong"=> $travellerLatestLoc[0]['longitude']??null,
									"travellerCurAddress"=> $travellerLatestLoc[0]['userAddress']??null,

								),
								"journeyDetails" => array(
									"toLat"=> $journeyDetails[0]['toLatitude']??null,   
									"toLong"=> $journeyDetails[0]['toLongitude']??null,
									"fromLat"=> $journeyDetails[0]['fromLatitude']??null,   
									"fromLong"=> $journeyDetails[0]['fromLongitude']??null,   
									"fromAddress"=> $journeyDetails[0]['fromAddress']??null,   
									"toAddress"=> $journeyDetails[0]['toAddress']??null,   
									"vehicleId"=> $journeyDetails[0]['vehicleId']??null,   
									"vehicleName"=> $getvehicleTypeIs[0]['vType']??null,  
									"vehicleNo"=> $checkVehicleData[0]['vehicleRegNo']??null   
								)
							);
							// FCM - start
							$userDetails['userHashId'] = $travellerId;
							$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
							if(!empty($getDeviceIds)){
								foreach ($getDeviceIds as $dKey => $fcmIds) {
									$msgTitle = "Driver is reaching out to you shortly!";
									$params = array('driverId'=>$driverId,'tripId'=>$tripId,'travellerId'=>$travellerId,'vehicleName'=>trim($vehicleName[0]['vType']));
									$eventType = "acceptTrip";
									$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
								}
							}
							// FCM - end
							$this->response([
								'responseCode' => 200,
								'responseMsg' => "Trip acceptance Details",
								'responseData' => $TripDetails
							], REST_Controller::HTTP_OK);
						}
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "trip cant be accepted - invalid parameters passed",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "No trip is requested or Invalid tripReqId",
					], REST_Controller::HTTP_OK);
				}
			}
		// ***************************************************************************************************************
			public function getDriverInfo_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
					'responseMsg' => "travellerId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$getDriverInfoIfTripIsAssigned = $this->Admin_model->getDriverInfoForTrip($tripId,$travellerId);
				if(!empty($getDriverInfoIfTripIsAssigned)){
					$driverId = $getDriverInfoIfTripIsAssigned[0]['driverId'];
					$driverLatestDetails = $this->Admin_model->getDriverLatestLoc($driverId);
					if(!empty($driverLatestDetails)){
						$driverImage =  base_url().'uploads/userImage/'.$driverLatestDetails[0]['userHashId'].'.png';
						$driverDetails = array(
							"driverDetails"=> array(
								"driverName"=> $driverLatestDetails[0]['userName']?? null,
								"driverEmail"=> $driverLatestDetails[0]['userEmail']?? null,
								"driverImage"=> $driverImage,
								"driverMobileNo"=> $driverLatestDetails[0]['signInMobile']?? null,
								"driverId"=> $driverLatestDetails[0]['userHashId']?? null,
								"driverCurLat"=> $driverLatestDetails[0]['latitude']?? null,
								"driverCurLong"=> $driverLatestDetails[0]['longitude']?? null
							)
						);
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Driver details Found",
							'responseData' => $driverDetails,
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Driver details not Found",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "No Driver Assigned for this trip",
					], REST_Controller::HTTP_OK);
				}
			}
		// ******************************************************************************************************************
			public function getTravellerInfo_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
					'responseMsg' => "driverId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$getTravellerInfoIfTripIsAssigned = $this->Admin_model->getTravellerInfoForTrip($tripId,$driverId);
				if(!empty($getTravellerInfoIfTripIsAssigned)){
					$travellerId = $getTravellerInfoIfTripIsAssigned[0]['userId'];
					$travellerLatestDetails = $this->Admin_model->getTravellerLatestLoc($travellerId);
					if(!empty($travellerLatestDetails)){

						$travellerImage =  base_url().'uploads/userImage/'.$travellerLatestDetails[0]['userHashId'].'.png';
						$travellerDetails = array(
							"travellerDetails"=> array(
								"travellerName"=> $travellerLatestDetails[0]['userName']?? null,
								"travellerEmail"=> $travellerLatestDetails[0]['userEmail']?? null,
								"travellerImage"=> $travellerImage,
								"travellerMobileNo"=> $travellerLatestDetails[0]['signInMobile']?? null,
								"travellerId"=> $travellerLatestDetails[0]['userHashId']?? null,
								"travellerCurLat"=> $travellerLatestDetails[0]['latitude']?? null,
								"travellerCurLong"=> $travellerLatestDetails[0]['longitude']?? null
							)
						);
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Traveller details Found",
							'responseData' => $travellerDetails,
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Traveller details not Found",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Traveller doesn't have any trip",
					], REST_Controller::HTTP_OK);
				}
			}

		// ******************************************************************************************************************

			public function rejectTrip_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
					'responseMsg' => "travellerId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
					'responseMsg' => "driverId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
					'responseMsg' => "driverId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$tripReqId = (!empty($inputArray['tripReqId'])?$inputArray['tripReqId']:$this->response([
					'responseMsg' => "tripReqId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$tripContent['travellerId'] = $travellerId; 
				$tripContent['driverId'] = $driverId;
				$tripContent['tripReqId'] = $tripReqId;
				$tripContent['tripStatus'] = 'requested';

				$tripIsReqOrNot = $this->Admin_model->checkTripAcceptedOrNot($travellerId,$tripReqId);

				if(!empty($tripIsReqOrNot)){
					$this->response([
						'responseCode' => 200,
						'responseMsg' => "Trip is rejected successfully",
					], REST_Controller::HTTP_OK);
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "No trip is requested or Invalid parameters passed",
					], REST_Controller::HTTP_OK);
				}
				// $checkTripAcceptedOrNot = $this->Admin_model->checkTripAcceptedOrNot($driverId,$travellerId);
				// $dbGenTripId = $this->Admin_model->createTrip($tripContent);
				// if(!empty($dbGenTripId)){
				// 	$updatedTripHash = $this->Admin_model->updateTripHashId($dbGenTripId);
				// 	if(!empty($updatedTripHash)){
				// 		$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($driverId);
				// 		$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);
				// 		if(empty($driverLatestLoc)||empty($travellerLatestLoc)){
				// 			$this->response([
				// 				'responseCode' => 400,
				// 				'responseMsg' => "trip cant be rejected - invalid driverId / travellerId passed",
				// 			], REST_Controller::HTTP_OK);
				// 		}else{
				// 			$this->response([
				// 				'responseCode' => 200,
				// 				'responseMsg' => "Trip is rejected successfully",
				// 			], REST_Controller::HTTP_OK);
				// 		}
				// 	}
				// }else{
				// 	$this->response([
				// 		'responseCode' => 400,
				// 		'responseMsg' => "Trip cant be rejected - invalid parameters passed",
				// 	], REST_Controller::HTTP_OK);
				// }
			}
		// ****************************************************************************************************************

		// startTrip(D) - (travellerId,driverId,tripId,TripOTP,driverPickUp lat/long) [orderId in FCM]

			public function startTrip_POST(){

				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$tripOtp = (!empty($inputArray['tripOtp'])?$inputArray['tripOtp']:$this->response([
					'responseMsg' => "tripOtp is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$driverPickupLatitude = (!empty($inputArray['driverPickupLatitude'])?$inputArray['driverPickupLatitude']:$this->response([
					'responseMsg' => "driverPickupLatitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$driverPickupLongitude = (!empty($inputArray['driverPickupLongitude'])?$inputArray['driverPickupLongitude']:$this->response([
					'responseMsg' => "driverPickupLongitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$getTripDetails = $this->Admin_model->getTripDetails($tripId);
				if(!empty($getTripDetails)){
					if($getTripDetails[0]['tripStatus']=='rejected'){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Trip is rejected already",
						], REST_Controller::HTTP_OK);
					}else{
						if($getTripDetails[0]['tripOtp']==$tripOtp){
							$driverTripLoc['driverFromLongitude'] = $driverPickupLongitude;
							$driverTripLoc['driverFromLatitude'] = $driverPickupLatitude;
							$driverTripLoc['tripStatus'] = 'started';
							$driverTripLoc['tripPickUpTime'] = time();
							$updateTripDetails = $this->Admin_model->updateTrip($tripId,$driverTripLoc);
							if(!empty($updateTripDetails)){
								$driverId = $getTripDetails[0]['driverId'];
								$travellerId = $getTripDetails[0]['userId'];
								//To Driver
								$userDetails['userHashId'] = $driverId;
								$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
								if(!empty($getDeviceIds)){
									foreach ($getDeviceIds as $dKey => $fcmIds) {
										$params = array('tripId'=>$tripId,"driverId"=>$driverId,"travellerId"=>$travellerId);
										$eventType = "startTrip"; //Used for driver when he confirms trips
										$msgTitle = "Trip is started ! Have a Safe Journey !";
										$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
									}
								}
								//To Traveller
								$userDetails['userHashId'] = $travellerId;
								$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
								if(!empty($getDeviceIds)){
									foreach ($getDeviceIds as $dKey => $fcmIds) {
										$params = array('tripId'=>$tripId,"driverId"=>$driverId,"travellerId"=>$travellerId);
										$eventType = "startTrip"; 
										$msgTitle = "Trip is started ! Have a Safe Journey !";
										$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
									}
								}

								$this->response([
									'responseCode' => 200,
									'responseMsg' => "Trip is started",
								], REST_Controller::HTTP_OK);
							}else{
								$this->response([
									'responseCode' => 400,
									'responseMsg' => "Trip cant be started - Invalid parameters passed",
								], REST_Controller::HTTP_OK);
							}
						}else{
							$this->response([
								'responseCode' => 400,
								'responseMsg' => "Trip OTP is invalid or incorrect",
							], REST_Controller::HTTP_OK);
						}
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Trip id is invalid or incorrect",
					], REST_Controller::HTTP_OK);
				}
			}

		// ****************************************************************************
			public function endTrip_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$driverDropLatitude = (!empty($inputArray['driverDropLatitude'])?$inputArray['driverDropLatitude']:$this->response([
					'responseMsg' => "driverDropLatitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$driverDropLongitude = (!empty($inputArray['driverDropLongitude'])?$inputArray['driverDropLongitude']:$this->response([
					'responseMsg' => "driverDropLongitude is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$getTripDetails = $this->Admin_model->getTripDetails($tripId);

				if(!empty($getTripDetails)){
					if($getTripDetails[0]['tripStatus']=='rejected'){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Trip is rejected already",
						], REST_Controller::HTTP_OK);
					}elseif($getTripDetails[0]['tripStatus']=='started'){
						$pmId = $getTripDetails[0]['pmId'];
						$paymentModeDetails = $this->Admin_model->getPaymentModeDetails($pmId);
						$paymentMode = $paymentModeDetails[0]['paymentMode'] ?? null;

						$fromLatitude = $getTripDetails[0]['driverFromLatitude'];
						$fromLongitude = $getTripDetails[0]['driverFromLongitude'];
						$toLatitude = $driverDropLatitude;
						$toLongitude = $driverDropLongitude;
						$distInKM =  $this->CalDis($fromLatitude,$fromLongitude,$toLatitude,$toLongitude,"K");
						$vehiclePricPerKM = $this->Admin_model->getVehicleDetailsForAmountCalc($tripId);
						$driverTripLoc['driverDropLatitude'] = $driverDropLatitude;
						$driverTripLoc['driverDropLongitude'] = $driverDropLongitude;
						$driverTripLoc['tripStatus'] = 'completed';
						$driverTripLoc['finalTripAmount'] = ceil($vehiclePricPerKM[0]['pricePerKM']*$distInKM);
						$driverTripLoc['tripDropTime'] = time();
						$driverTripLoc['orderId'] = $orderId = md5($tripId);
						$updateTripDetails = $this->Admin_model->updateTrip($tripId,$driverTripLoc);
						if(!empty($updateTripDetails)){
							$driverId = $getTripDetails[0]['driverId'];
							$travellerId = $getTripDetails[0]['userId'];

							//To Traveller
							$userDetails['userHashId'] = $travellerId;
							$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
							if(!empty($getDeviceIds)){
								foreach ($getDeviceIds as $dKey => $fcmIds) {
									$msgTitle = "Yo Rides trip total amount - Rs. ".$driverTripLoc['finalTripAmount'];
									$params = array(
										'tripId'=>$tripId,
										"driverDropLatitude"=>$driverTripLoc['driverDropLatitude'],
										"driverDropLongitude"=>$driverTripLoc['driverDropLongitude'],
										"travellerId"=>$travellerId,
										"driverId"=>$driverId,
										"orderId"=>$orderId,
										"paymentMode"=>$paymentMode,
										"pmId" =>$pmId,
										"finalTripAmount" => $driverTripLoc['finalTripAmount']
									);
									$eventType = "endTrip";
									$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
								}
							}

							//To Driver
							$userDetails['userHashId'] = $driverId;
							$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
							if(!empty($getDeviceIds)){
								foreach ($getDeviceIds as $dKey => $fcmIds) {
									$msgTitle = "Thanks for dropping! Trip is completed";
									$params = array(
										'tripId'=>$tripId,
										"driverDropLatitude"=>$driverTripLoc['driverDropLatitude'],
										"driverDropLongitude"=>$driverTripLoc['driverDropLongitude'],
										"travellerId"=>$travellerId,
										"driverId"=>$driverId,
										"orderId"=>$orderId,
										"paymentMode"=>$paymentMode,
										"pmId" =>$pmId,
										"finalTripAmount" => $driverTripLoc['finalTripAmount']
									);
									$eventType = "endTrip";
									$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
								}
							}
							$this->response([
								'responseMsg' => "trip completed status is updated",
								'responseCode' => 200,
								'responseData' => array(
									"finalTripAmount" => $driverTripLoc['finalTripAmount'],
									'tripId'=>$tripId,
									"paymentMode"=>$paymentMode,
									"pmId" =>$pmId,
									"orderId"=>$orderId
								),
							], REST_Controller::HTTP_BAD_REQUEST);
						}else{
							$this->response([
								'responseMsg' => "Invalid Params passed !",
								'responseCode' => 400,
							], REST_Controller::HTTP_BAD_REQUEST);
						}
					}else{
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is already completed",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Trip id is invalid or incorrect",
					], REST_Controller::HTTP_OK);
				}
			}

		// ***************************************************************************************************************

		// startTrip(D) - (travellerId,driverId,tripId,TripOTP,driverPickUp lat/long) [orderId in FCM]

			public function addAccountDetails_POST(){

				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$bankName = (!empty($inputArray['bankName'])?$inputArray['bankName']:$this->response([
					'responseMsg' => "bankName is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$bankAccountNo = (!empty($inputArray['bankAccountNo'])?$inputArray['bankAccountNo']:$this->response([
					'responseMsg' => "bankAccountNo is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$bankIfsc = (!empty($inputArray['bankIfsc'])?$inputArray['bankIfsc']:$this->response([
					'responseMsg' => "bankIfsc is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$accHolderName = (!empty($inputArray['accHolderName'])?$inputArray['accHolderName']:$this->response([
					'responseMsg' => "accHolderName is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$userDetails['userHashId'] = $userId;
				$checkUserExist = $this->Admin_model->checkUserData($userDetails);

				if(!empty($checkUserExist)){
					$userBankDetails['bankName'] = $bankName;
					$userBankDetails['bankAccountNo'] = $bankAccountNo;
					$userBankDetails['bankIfsc'] = $bankIfsc;
					$userBankDetails['accHolderName'] = $accHolderName;
					$updateBankDetails = $this->Admin_model->updateBankDetails($userBankDetails,$userId);
					if(!empty($updateBankDetails)){
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Bank Details were updated",
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Bank Details were not updated! Try Again",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "userId is invalid or incorrect",
					], REST_Controller::HTTP_OK);
				}
			}

		// ****************************************************************************

			public function signOut_POST(){

				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$userData['userHashId'] = $userId;
				$checkUserExist = $this->Admin_model->checkUserExist($userData);
				if(!empty($checkUserExist)){
					$deviceData['allowSignIn'] = 0;
					$updateDeviceDetails = $this->Admin_model->updateDeviceDetails($deviceData,$userId);
					if(!empty($updateDeviceDetails)){
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "sign out is enabled now, redirect to sign in page",
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "user not exist",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "user not exist",
					], REST_Controller::HTTP_OK);
				}
			}

		// *******************************************************************

			public function getTripDetails_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
					'responseMsg' => "travellerId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$checkTripIsReqOrNot = $this->Admin_model->checkTripIsReqOrNot($travellerId,$tripId);
				if(!empty($checkTripIsReqOrNot)){

					if($checkTripIsReqOrNot[0]['tripStatus']=='requested'){
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is requested by traveller ! Awaiting for driver to accept",
						], REST_Controller::HTTP_OK);
					}elseif($checkTripIsReqOrNot[0]['tripStatus']=='accepted') {
						$driverId = $checkTripIsReqOrNot[0]['driverId'];
						$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);
						$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($driverId);
						$journeyDetails = $this->Admin_model->getJourneyDetails($tripId);
						if(!empty($journeyDetails)){
							$vehicleId = $journeyDetails[0]['vehicleId']??null;
							$getvehicleTypeIs = $this->Admin_model->getVehicleConfig($vehicleId);
						}
						$vData['userHashId'] = $driverId;
						$checkVehicleData = $this->Admin_model->checkVehicleData($vData);
						$driverImage =  base_url().'uploads/userImage/'.$driverLatestLoc[0]['userHashId'].'.png';
						$travellerImage =  base_url().'uploads/userImage/'.$travellerLatestLoc[0]['userHashId'].'.png';
						$TripDetails = array(
							"tripId" => $checkTripIsReqOrNot[0]['tripId'],
							"driverDetails"=> array(
								"driverName"=> $driverLatestLoc[0]['userName']?? null,
								"driverEmail"=> $driverLatestLoc[0]['userEmail']?? null,
								"driverImage"=> $driverImage,
								"driverMobileNo"=> $driverLatestLoc[0]['signInMobile']?? null,
								"driverId"=> $driverLatestLoc[0]['userHashId']?? null,
								"driverCurLat"=> $driverLatestLoc[0]['latitude']?? null,
								"driverCurLong"=> $driverLatestLoc[0]['longitude']?? null
							),
							"travellerDetails"=> array(
								"travellerName"=> $travellerLatestLoc[0]['userName']??null,
								"travellerEmail"=> $travellerLatestLoc[0]['userEmail']??null,
								"travellerImage"=> $travellerImage,
								"travellerMobileNo"=> $travellerLatestLoc[0]['signInMobile']??null,
								"travellerId"=> $travellerLatestLoc[0]['userHashId']??null,
								"travellerCurLat"=> $travellerLatestLoc[0]['latitude']??null,
								"travellerCurLong"=> $travellerLatestLoc[0]['longitude']??null
							),
							"journeyDetails" => array(
								"toLat"=> $journeyDetails[0]['toLatitude']??null,   
								"toLong"=> $journeyDetails[0]['toLongitude']??null,
								"fromLat"=> $journeyDetails[0]['fromLatitude']??null,   
								"fromLong"=> $journeyDetails[0]['fromLongitude']??null,   
								"fromAddress"=> $journeyDetails[0]['fromAddress']??null,   
								"toAddress"=> $journeyDetails[0]['toAddress']??null,   
								"vehicleId"=> $journeyDetails[0]['vehicleId']??null,   
								"vehicleName"=> $getvehicleTypeIs[0]['vType']??null,  
								"vehicleNo"=> $checkVehicleData[0]['vehicleRegNo']??null   
							)
						);
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is accepted by driver ! Driver is on the way to pick up !",
							'responseData' => $TripDetails
						], REST_Controller::HTTP_OK);

					}elseif($checkTripIsReqOrNot[0]['tripStatus']=='started') {
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is started by driver ! Driver is on the way to drop the traveller !",
						], REST_Controller::HTTP_OK);
					}elseif($checkTripIsReqOrNot[0]['tripStatus']=='completed') {

						$driverId = $checkTripIsReqOrNot[0]['driverId'];
						$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);
						$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($driverId);
						$journeyDetails = $this->Admin_model->getJourneyDetails($tripId);
						if(!empty($journeyDetails)){
							$vehicleId = $journeyDetails[0]['vehicleId']??null;
							$getvehicleTypeIs = $this->Admin_model->getVehicleConfig($vehicleId);
						}
						$vData['userHashId'] = $driverId;
						$checkVehicleData = $this->Admin_model->checkVehicleData($vData);
						$driverImage =  base_url().'uploads/userImage/'.$driverLatestLoc[0]['userHashId'].'.png';
						$travellerImage =  base_url().'uploads/userImage/'.$travellerLatestLoc[0]['userHashId'].'.png';
						$TripDetails = array(
							"tripId" => $checkTripIsReqOrNot[0]['tripId'],
							"driverDetails"=> array(
								"driverName"=> $driverLatestLoc[0]['userName']?? null,
								"driverEmail"=> $driverLatestLoc[0]['userEmail']?? null,
								"driverImage"=> $driverImage,
								"driverMobileNo"=> $driverLatestLoc[0]['signInMobile']?? null,
								"driverId"=> $driverLatestLoc[0]['userHashId']?? null,
								"driverCurLat"=> $driverLatestLoc[0]['latitude']?? null,
								"driverCurLong"=> $driverLatestLoc[0]['longitude']?? null
							),
							"travellerDetails"=> array(
								"travellerName"=> $travellerLatestLoc[0]['userName']??null,
								"travellerEmail"=> $travellerLatestLoc[0]['userEmail']??null,
								"travellerImage"=> $travellerImage,
								"travellerMobileNo"=> $travellerLatestLoc[0]['signInMobile']??null,
								"travellerId"=> $travellerLatestLoc[0]['userHashId']??null,
								"travellerCurLat"=> $travellerLatestLoc[0]['latitude']??null,
								"travellerCurLong"=> $travellerLatestLoc[0]['longitude']??null
							),
							"journeyDetails" => array(
								"toLat"=> $journeyDetails[0]['toLatitude']??null,   
								"toLong"=> $journeyDetails[0]['toLongitude']??null,
								"fromLat"=> $journeyDetails[0]['fromLatitude']??null,   
								"fromLong"=> $journeyDetails[0]['fromLongitude']??null,   
								"fromAddress"=> $journeyDetails[0]['fromAddress']??null,   
								"toAddress"=> $journeyDetails[0]['toAddress']??null,   
								"vehicleId"=> $journeyDetails[0]['vehicleId']??null,   
								"vehicleName"=> $getvehicleTypeIs[0]['vType']??null,  
								"vehicleNo"=> $checkVehicleData[0]['vehicleRegNo']??null   
							)
						);
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is completed",
							'responseData' => $TripDetails
						], REST_Controller::HTTP_OK);
					}elseif($checkTripIsReqOrNot[0]['tripStatus']=='cancelledByTraveller') {
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is cancelled by traveller",
						], REST_Controller::HTTP_OK);
					}elseif($checkTripIsReqOrNot[0]['tripStatus']=='cancelledByDriver') {
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is cancelled by Driver",
						], REST_Controller::HTTP_OK);
					}

				}
			}

		// ****************************************************************************
			public function searchingForDriver_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
					'responseMsg' => "travellerId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$tripReqId = (!empty($inputArray['tripReqId'])?$inputArray['tripReqId']:$this->response([
					'responseMsg' => "tripReqId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$checkTripIsReqOrNot = $this->Admin_model->checkTripIsReqOrNotUsingTripReqId($travellerId,$tripReqId);

				if(!empty($checkTripIsReqOrNot)){
					$checkAnyDriverConfirmedTrip = $this->Admin_model->checkAnyDriverConfirmedTrip($travellerId);
					if(!empty($checkAnyDriverConfirmedTrip)){
						$driverId = $checkAnyDriverConfirmedTrip[0]['driverId'];
						$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);
						$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($driverId);
						$journeyDetails = $this->Admin_model->getJourneyDetails($tripReqId);
						if(!empty($journeyDetails)){
							$vehicleId = $journeyDetails[0]['vehicleId']??null;
							$getvehicleTypeIs = $this->Admin_model->getVehicleConfig($vehicleId);
						}
						$vData['userHashId'] = $driverId;
						$checkVehicleData = $this->Admin_model->checkVehicleData($vData);
						$driverImage =  base_url().'uploads/userImage/'.$driverLatestLoc[0]['userHashId'].'.png';
						$travellerImage =  base_url().'uploads/userImage/'.$travellerLatestLoc[0]['userHashId'].'.png';

						$TripDetails = array(
							"tripId" => $checkAnyDriverConfirmedTrip[0]['tripId'],
							"driverDetails"=> array(
								"driverName"=> $driverLatestLoc[0]['userName']?? null,
								"driverEmail"=> $driverLatestLoc[0]['userEmail']?? null,
								"driverImage"=> $driverImage,
								"driverMobileNo"=> $driverLatestLoc[0]['signInMobile']?? null,
								"driverId"=> $driverLatestLoc[0]['userHashId']?? null,
								"driverCurLat"=> $driverLatestLoc[0]['latitude']?? null,
								"driverCurLong"=> $driverLatestLoc[0]['longitude']?? null
							),
							"travellerDetails"=> array(
								"travellerName"=> $travellerLatestLoc[0]['userName']??null,
								"travellerEmail"=> $travellerLatestLoc[0]['userEmail']??null,
								"travellerImage"=> $travellerImage,
								"travellerMobileNo"=> $travellerLatestLoc[0]['signInMobile']??null,
								"travellerId"=> $travellerLatestLoc[0]['userHashId']??null,
								"travellerCurLat"=> $travellerLatestLoc[0]['latitude']??null,
								"travellerCurLong"=> $travellerLatestLoc[0]['longitude']??null
							),
							"journeyDetails" => array(
								"toLat"=> $journeyDetails[0]['toLatitude']??null,   
								"toLong"=> $journeyDetails[0]['toLongitude']??null,
								"fromLat"=> $journeyDetails[0]['fromLatitude']??null,   
								"fromLong"=> $journeyDetails[0]['fromLongitude']??null,   
								"fromAddress"=> $journeyDetails[0]['fromAddress']??null,   
								"toAddress"=> $journeyDetails[0]['toAddress']??null,   
								"vehicleId"=> $journeyDetails[0]['vehicleId']??null,   
								"vehicleName"=> $getvehicleTypeIs[0]['vType']??null,  
								"vehicleNo"=> $checkVehicleData[0]['vehicleRegNo']??null   
							)
						);
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip acceptance Details",
							'responseData' => $TripDetails
						], REST_Controller::HTTP_OK);

					}else{
						$this->response([
							'responseCode' => 201,
							'responseMsg' => "drivers not yet accepted the trip",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "traveller have not requested for any trip or travellerId not found",
					], REST_Controller::HTTP_OK);
				}
			}

		// ****************************************************************************************************

			public function driverReachingTime_POST(){

				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST)); 

				$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
					'responseMsg' => "driverId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
					'responseMsg' => "travellerId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$driverCurrLoc = $this->Admin_model->getDriverLatestLoc($driverId);
				$travellerCurrLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);

				if((!empty($driverCurrLoc))and(!empty($travellerCurrLoc))){
					$fromLatitude = $driverCurrLoc[0]['latitude'];
					$fromLongitude = $driverCurrLoc[0]['longitude'];
					$toLatitude = $travellerCurrLoc[0]['latitude'];
					$toLongitude = $travellerCurrLoc[0]['longitude'];
					$distInKM = $this->CalDis($fromLatitude,$fromLongitude,$toLatitude,$toLongitude,"K");
					if($distInKM<1){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "driver is reached your location",
						], REST_Controller::HTTP_OK);
					}else{
						$distInKM = number_format($distInKM,'2');
						$time = ceil(number_format(($distInKM/15),'2')*60);
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "driver is reaching your location",
							'responseData' => array("reachingTimeInMin"=>$time,"driverDetails"=>$driverCurrLoc),
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "driverId/travellerId is invalid",
					], REST_Controller::HTTP_OK);
				}
			}

		// *********************************************************************************
			public function privacyPolicy_GET(){
				$policyDoc = base_url().'uploads/yoRidesDocuments/policyDoc.pdf';
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "Policy Document Details",
					'responseData' => $policyDoc,
				], REST_Controller::HTTP_OK);


			}
			// *****************************************************************************
			public function travellerCancellingTrip_post(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST)); 
				$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
					'responseMsg' => "travellerId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$getTripDetails = $this->Admin_model->getTripDetails($tripId);

				if(!empty($getTripDetails)){
					if($getTripDetails[0]['tripStatus']=='completed'){
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is completed already",
						], REST_Controller::HTTP_OK);
					}elseif($getTripDetails[0]['tripStatus']=='started'){
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is started already",
						], REST_Controller::HTTP_OK);
					}elseif($getTripDetails[0]['tripStatus']=='accepted'){
						$driverTripLoc['tripStatus'] = 'cancelledByTraveller';
						$updateTripDetails = $this->Admin_model->updateTrip($tripId,$driverTripLoc);
						if(!empty($updateTripDetails)){
							$driverId = $getTripDetails[0]['driverId'];
							$travellerId = $getTripDetails[0]['userId'];

							//To Traveller
							// $userDetails['userHashId'] = $travellerId;
							// $getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
							// if(!empty($getDeviceIds)){
							// 	foreach ($getDeviceIds as $dKey => $fcmIds) {
							// 		$msgTitle = "Yo Rides trip is cancelled successfully !";
							// 		$params = array(
							// 			'tripId'=>$tripId,
							// 			"driverId"=>$driverId,
							// 			"travellerId"=>$travellerId
							// 		);
							// 		$eventType = "cancelTrip";
							// 		$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
							// 	}
							// }

							//To Driver
							$userDetails['userHashId'] = $driverId;
							$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
							if(!empty($getDeviceIds)){
								foreach ($getDeviceIds as $dKey => $fcmIds) {
									$msgTitle = "Yo Rides Trip is cancelled by the traveller !";
									$params = array(
										'tripId'=>$tripId,
										"travellerId"=>$travellerId,
										"driverId"=>$driverId,
									);
									$eventType = "cancelTrip";
									$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
								}
							}
							$this->response([
								'responseCode' => 200,
								'responseMsg' => "Trip is cancelled successfully by Traveller",
							], REST_Controller::HTTP_BAD_REQUEST);
						}else{
							$this->response([
								'responseMsg' => "Invalid Params passed !",
								'responseCode' => 400,
							], REST_Controller::HTTP_BAD_REQUEST);
						}
					}else{
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is already completed",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Trip id is invalid or incorrect",
					], REST_Controller::HTTP_OK);
				}
			}
		// *****************************************************************************
			public function driverCancellingTrip_post(){

				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST)); 
				$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
					'responseMsg' => "driverId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$getTripDetails = $this->Admin_model->getTripDetails($tripId);

				if(!empty($getTripDetails)){
					if($getTripDetails[0]['tripStatus']=='completed'){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Trip is completed already",
						], REST_Controller::HTTP_OK);
					}elseif($getTripDetails[0]['tripStatus']=='started'){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Trip is started already",
						], REST_Controller::HTTP_OK);
					}elseif($getTripDetails[0]['tripStatus']=='accepted'){
						$driverTripLoc['tripStatus'] = 'cancelledByDriver';
						$updateTripDetails = $this->Admin_model->updateTrip($tripId,$driverTripLoc);
						if(!empty($updateTripDetails)){
							$driverId = $getTripDetails[0]['driverId'];
							$travellerId = $getTripDetails[0]['userId'];

							//To Driver
							// $userDetails['userHashId'] = $driverId;
							// $getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
							// if(!empty($getDeviceIds)){
							// 	foreach ($getDeviceIds as $dKey => $fcmIds) {
							// 		$msgTitle = "Yo Rides Trip is cancelled successfully!";
							// 		$params = array(
							// 			'tripId'=>$tripId,
							// 			"travellerId"=>$travellerId,
							// 			"driverId"=>$driverId,
							// 		);
							// 		$eventType = "cancelTrip";
							// 		$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
							// 	}
							// }

							//To Traveller
							$userDetails['userHashId'] = $travellerId;
							$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
							if(!empty($getDeviceIds)){
								foreach ($getDeviceIds as $dKey => $fcmIds) {
									$msgTitle = "Yo Rides Trip is cancelled by the driver !";
									$params = array(
										'tripId'=>$tripId,
										"driverId"=>$driverId,
										"travellerId"=>$travellerId
									);
									$eventType = "cancelTrip";
									$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
								}
							}
							$this->response([
								'responseCode' => 200,
								'responseMsg' => "Trip is cancelled successfully by Driver",
							], REST_Controller::HTTP_BAD_REQUEST);
						}else{
							$this->response([
								'responseMsg' => "Invalid Params passed !",
								'responseCode' => 400,
							], REST_Controller::HTTP_BAD_REQUEST);
						}
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Trip is already completed",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Trip id is invalid or incorrect",
					], REST_Controller::HTTP_OK);
				}

			}

		//*****************************************************************

			public function driverReached_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST)); 
				$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
					'responseMsg' => "driverId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$driverCurLat = (!empty($inputArray['driverCurLat'])?$inputArray['driverCurLat']:$this->response([
					'responseMsg' => "driverCurLat is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$driverCurLong = (!empty($inputArray['driverCurLong'])?$inputArray['driverCurLong']:$this->response([
					'responseMsg' => "driverCurLong is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$getTripDetails = $this->Admin_model->getTripDetails($tripId);
				if(!empty($getTripDetails)){
					$travellerId = $getTripDetails[0]['userId'];
					// FCM - start
					$userDetails['userHashId'] = $travellerId;
					$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
					if(!empty($getDeviceIds)){
						foreach ($getDeviceIds as $dKey => $fcmIds) {
							$msgTitle = "Driver is reached to your location !";
							$params = array('driverId'=>$driverId,'tripId'=>$tripId,'travellerId'=>$travellerId,'driverCurLat'=>$driverCurLat,'driverCurLong'=>$driverCurLong);
							$eventType = "driverReached";
							$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
						}
					}
					$this->response([
						'responseCode' => 200,
						'responseMsg' => "FCM is sent to traveller regarding driver arrival to pick up",
					], REST_Controller::HTTP_OK);
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Trip id is invalid or incorrect params passed",
					], REST_Controller::HTTP_OK);
				}
			}

		// *********************************************************************************************

			public function updateTripPaymentStatus_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$orderId = (!empty($inputArray['orderId'])?$inputArray['orderId']:$this->response([
					'responseMsg' => "orderId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST)); 

				$paymentMode = (!empty($inputArray['paymentMode'])?$inputArray['paymentMode']:$this->response([
					'responseMsg' => "paymentMode is required. Either 'cash' or 'digital'",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$isPaymentDone = (!empty($inputArray['isPaymentDone'])?$inputArray['isPaymentDone']:$this->response([
					'responseMsg' => "isPaymentDone is required. Either yes or no",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				if(!empty($inputArray['razorpayOrderId'])){
					$razorpayOrderId = $inputArray['razorpayOrderId'];
				}else{
					$razorpayOrderId = "";
				}
				if(!empty($inputArray['pmId'])){
					$pmId = $inputArray['pmId'];
				}else{
					$pmId = "";
				}
				$getTripDetails = $this->Admin_model->getTripDetails($tripId);
				if(!empty($getTripDetails)){
					$paymentDetails['orderId'] = $orderId;
					$paymentDetails['paymentMode'] = $paymentMode;
					$paymentDetails['isPaymentDone'] = $isPaymentDone;
					$paymentDetails['razorpayOrderId'] = $razorpayOrderId;
					$paymentDetails['pmId'] = $pmId;
					$paymentDetails['tripId'] = $tripId;
					$paymentDetails['paymentDate'] = time();
					$paymentDetailsAdded = $this->Admin_model->storePaymentData($paymentDetails);
					if(!empty($paymentDetailsAdded)){

						$driverId = $getTripDetails[0]['driverId'];
						$travellerId = $getTripDetails[0]['userId'];

						//To Traveller
						$userDetails['userHashId'] = $travellerId;
						$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
						if(!empty($getDeviceIds)){
							foreach ($getDeviceIds as $dKey => $fcmIds) {
								$msgTitle = "Payment is successfully updated !";
								$params = array(
									'orderId'=>$orderId
								);
								$eventType = "updateTripPaymentStatus";
								$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
							}
						}

						// To Driver
						$userDetails['userHashId'] = $driverId;
						$getDeviceIds[] = $this->Admin_model->checkUserData($userDetails)[0]['fcmToken'];
						if(!empty($getDeviceIds)){
							foreach ($getDeviceIds as $dKey => $fcmIds) {
								$msgTitle = "Payment is successfully updated !";
								$params = array(
									'orderId'=>$orderId
								);
								$eventType = "updateTripPaymentStatus";
								$sendFCM = $this->sendFCM($msgTitle,$fcmIds,$params,$eventType);
							}
						}
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Payment details is updated successfully",
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Trip id is invalid or incorrect params passed",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Trip id is invalid or incorrect params passed",
					], REST_Controller::HTTP_OK);
				}
			}


		// ************************************************************************************

			public function getPaymentStatus_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$orderId = (!empty($inputArray['orderId'])?$inputArray['orderId']:$this->response([
					'responseMsg' => "orderId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST)); 

				if(!empty($inputArray['driverId'])){
					$driverId = $inputArray['driverId'];
				}else{
					$driverId = "";
				}
				if(!empty($inputArray['travellerId'])){
					$travellerId = $inputArray['travellerId'];
				}else{
					$travellerId = "";
				}
				$tripIdfromDB = $this->Admin_model->getTripIdUsingOrderId($orderId);
				// journey details - start
				$journeyDetails = $this->Admin_model->getJourneyDetails($tripIdfromDB[0]['tripId']);
				if(!empty($journeyDetails)){
					$vehicleId = $journeyDetails[0]['vehicleId']??null;
					$getvehicleTypeIs = $this->Admin_model->getVehicleConfig($vehicleId);
				}
				// if(!empty($driverId)){
				$vData['userHashId'] = $journeyDetails[0]['driverId'];
				$checkVehicleData = $this->Admin_model->checkVehicleData($vData);
				// }
				$journeyDetailsArr = array("journeyDetails" => array(
					"toLat"=> $journeyDetails[0]['toLatitude']??null,   
					"toLong"=> $journeyDetails[0]['toLongitude']??null,
					"fromLat"=> $journeyDetails[0]['fromLatitude']??null,   
					"fromLong"=> $journeyDetails[0]['fromLongitude']??null,   
					"fromAddress"=> $journeyDetails[0]['fromAddress']??null,   
					"toAddress"=> $journeyDetails[0]['toAddress']??null,   
					"vehicleId"=> $journeyDetails[0]['vehicleId']??null,   
					"vehicleName"=> $getvehicleTypeIs[0]['vType']??null,  
					"vehicleNo"=> $checkVehicleData[0]['vehicleRegNo']??null   
				));
				// journey details - end

				$pmId = $journeyDetails[0]['pmId'];
				$tripPaymentMode = $this->Admin_model->getPaymentModeDetails($pmId);

				$billingDetailsArr = array("billingDetails"=>array(
					"finalTripAmount" => (int)$journeyDetails[0]['finalTripAmount'] ?? null,
					"roundedOffFinalTripAmount" => (int)0,
					"totalAmountIncludingTax" => (int)ceil($journeyDetails[0]['finalTripAmount']),
					"paymentMode"=> $tripPaymentMode[0]['paymentMode']
				));

				$journeyAndBillingArr = array_merge($journeyDetailsArr,$billingDetailsArr);
				$tripId = $tripIdfromDB[0]['tripId'];
				$getTripDetails = $this->Admin_model->getTripDetails($tripId);
				$dbTravellerId = $getTripDetails[0]['userId'];
				$dbDriverId = $getTripDetails[0]['driverId'];

				//driver detils - start
				if(!empty($travellerId)){
					$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($dbDriverId);
					$driverImage =  base_url().'uploads/userImage/'.$driverLatestLoc[0]['userHashId'].'.png';
					$driverDetailsArr = array(
						"driverDetails"=> array(
							"driverName"=> $driverLatestLoc[0]['userName']?? null,
							"driverEmail"=> $driverLatestLoc[0]['userEmail']?? null,
							"driverImage"=> $driverImage,
							"driverMobileNo"=> $driverLatestLoc[0]['signInMobile']?? null,
							"driverId"=> $driverLatestLoc[0]['userHashId']?? null,
							"driverCurLat"=> $driverLatestLoc[0]['latitude']?? null,
							"driverCurLong"=> $driverLatestLoc[0]['longitude']?? null
						));
					$res = array_merge($journeyAndBillingArr,$driverDetailsArr);
				}
				//driver detils - end
				//travellerdetails - start
				elseif(!empty($driverId)){
					$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($dbTravellerId);
					$travellerImage =  base_url().'uploads/userImage/'.$travellerLatestLoc[0]['userHashId'].'.png';
					$travellerDetailsArr = array("travellerDetails"=> array(
						"travellerName"=> $travellerLatestLoc[0]['userName']??null,
						"travellerEmail"=> $travellerLatestLoc[0]['userEmail']??null,
						"travellerImage"=> $travellerImage,
						"travellerMobileNo"=> $travellerLatestLoc[0]['signInMobile']??null,
						"travellerId"=> $travellerLatestLoc[0]['userHashId']??null,
						"travellerCurLat"=> $travellerLatestLoc[0]['latitude']??null,
						"travellerCurLong"=> $travellerLatestLoc[0]['longitude']??null
					));
				//travellerdetails - end
					$res = array_merge($journeyAndBillingArr,$travellerDetailsArr);
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "driverId or travellerId is required",
					], REST_Controller::HTTP_OK);
				}
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "payment status details were obtained successfully !",
					'responseData' => $res,
				], REST_Controller::HTTP_OK);
			}
			// **************************************************************************************************

			public function cancelTripSearch_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$tripReqId = (!empty($inputArray['tripReqId'])?$inputArray['tripReqId']:$this->response([
					'responseMsg' => "tripReqId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST)); 

				$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
					'responseMsg' => "travellerId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$getTripReqDetails = $this->Admin_model->getJourneyDetailsUsingTripReqId($tripReqId);
				if(!empty($getTripReqDetails)){
					if($getTripReqDetails[0]['tripStatus']=="requested"){
						$delTripReq = $this->Admin_model->deleteTripReqByTravellerUsingTripReqId($tripReqId);
						if(!empty($delTripReq)){
							$this->response([
								'responseCode' => 200,
								'responseMsg' => "trip search is cancelled successfully !",
							], REST_Controller::HTTP_OK);
						}else{
							$this->response([
								'responseCode' => 400,
								'responseMsg' => "Invalid tripReqId or travellerId",
							], REST_Controller::HTTP_OK);
						}
					}elseif($getTripReqDetails[0]['tripStatus']=="started"){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "trip is already started",
						], REST_Controller::HTTP_OK);
					}elseif($getTripReqDetails[0]['tripStatus']=="completed"){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "trip is already completed",
						], REST_Controller::HTTP_OK);
					}elseif($getTripReqDetails[0]['tripStatus']=="cancelledByTraveller"){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "trip is already cancelledByTraveller",
						], REST_Controller::HTTP_OK);
					}elseif($getTripReqDetails[0]['tripStatus']=="accepted"){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "trip is already accepted",
						], REST_Controller::HTTP_OK);
					}elseif($getTripReqDetails[0]['tripStatus']=="cancelledByDriver"){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "trip is already cancelledByDriver",
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Invalid tripReqId or travellerId",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Invalid tripReqId or travellerId",
					], REST_Controller::HTTP_OK);
				}
			}

		// *************************************************************************************************************

			
			public function getTripHistory_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				if(!empty($inputArray['driverId'])){
					$driverId = $inputArray['driverId'];
				}else{
					$driverId = "";
				}
				if(!empty($inputArray['travellerId'])){
					$travellerId = $inputArray['travellerId'];
				}else{
					$travellerId = "";
				}
				$tripModeFromPost = (!empty($inputArray['tripMode'])?$inputArray['tripMode']:$this->response([
					'responseMsg' => "tripMode is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				// using driverId
				if(!empty($driverId)){
					if($tripModeFromPost=='past'){
						$tripMode = array('completed','cancelledByDriver','cancelledByTraveller');
					}elseif ($tripModeFromPost=='upcoming') {
						$tripMode = array('requested');
					}elseif ($tripModeFromPost=='ongoing') {
						$tripMode = array('accepted','started','requested');
					}
					$getListOfTripsData	= $this->Admin_model->getTripHistoryUsingDriverId($driverId,$tripMode);
					if(!empty($getListOfTripsData)){
						$getVehicleList = $this->Admin_model->getAllVehicleList();
						foreach ($getVehicleList as $vKeys => $vValues) {
							$vehiclesDetails[$vValues['vType']] = $vValues['vehicleId'];
						}	
						foreach ($getListOfTripsData as $tripKey => $tripValue) {
							$pastTripResponse[$tripKey]['tripId'] = $tripValue['tripId'];
							$pastTripResponse[$tripKey]['vehicleId'] = $tripValue['vehicleId']??null;
							$pastTripResponse[$tripKey]['orderId'] = $tripValue['orderId']?? null;
							$pastTripResponse[$tripKey]['tripDate'] = date('m.d.Y h:i A', (int)$tripValue['tripPickUpTime']);
							$pastTripResponse[$tripKey]['toAddress'] = $tripValue['toAddress']??null;
							$vehType = array_search($tripValue['vehicleId'], $vehiclesDetails);
							if($vehType!=null){$vehTypeRes = $vehType;}else{$vehTypeRes = "";}
							$pastTripResponse[$tripKey]['vehicleType'] = $vehTypeRes;
							$pastTripResponse[$tripKey]['finalTripAmount'] = $tripValue['finalTripAmount']??null;
							$pastTripResponse[$tripKey]['isCancelled'] = ($tripValue['tripStatus']=='cancelledByTraveller')?true:($tripValue['tripStatus']=='cancelledByDriver')?true:false;
						}
						$this->response([
							'responseCode' => 200,
							'responseMsg' => ucwords($tripModeFromPost)." Trip History details",
							'responseData' => $pastTripResponse
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "No Trips yet",
						], REST_Controller::HTTP_OK);
					}
					// using travellerId
				}elseif (!empty($travellerId)) {
					if($tripModeFromPost=='past'){
						$tripMode = array('completed','cancelledByDriver','cancelledByTraveller');
					}elseif ($tripModeFromPost=='upcoming') {
						$tripMode = array('requested');
					}elseif ($tripModeFromPost=='ongoing') {
						$tripMode = array('accepted','started','requested');
					}
					$getListOfTripsDataUsingTravellerId	= $this->Admin_model->getTripHistoryUsingTravellerId($travellerId,$tripMode);
					if(!empty($getListOfTripsDataUsingTravellerId)){
						$getVehicleList = $this->Admin_model->getVehicleList();
						foreach ($getVehicleList as $vKeys => $vValues) {
							$vehiclesDetails[$vValues['vType']] = $vValues['vehicleId'];
						}	
						foreach ($getListOfTripsDataUsingTravellerId as $tripKey => $tripValue) {
							$pastTripResponse[$tripKey]['tripId'] = $tripValue['tripId'];
							$pastTripResponse[$tripKey]['vehicleId'] = $tripValue['vehicleId']??null;
							$pastTripResponse[$tripKey]['orderId'] = $tripValue['orderId']?? null;
							$pastTripResponse[$tripKey]['tripDate'] = date('m.d.Y h:i A', (int)$tripValue['tripPickUpTime']);
							$pastTripResponse[$tripKey]['toAddress'] = $tripValue['toAddress']??null;
							$vehType = array_search($tripValue['vehicleId'], $vehiclesDetails);
							if($vehType!=null){$vehTypeRes = $vehType;}else{$vehTypeRes = "";}
							$pastTripResponse[$tripKey]['vehicleType'] = $vehTypeRes;
							$pastTripResponse[$tripKey]['finalTripAmount'] = $tripValue['finalTripAmount']??null;
							$pastTripResponse[$tripKey]['isCancelled'] = ($tripValue['tripStatus']=='cancelledByTraveller')?true:($tripValue['tripStatus']=='cancelledByDriver')?true:false;
						}
						$this->response([
							'responseCode' => 200,
							'responseMsg' => ucwords($tripModeFromPost)." Trip History details",
							'responseData' => $pastTripResponse
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "No Trips yet",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "driverId or travellerId is required",
					], REST_Controller::HTTP_OK);
				}
			}

	// ***************************************************************************************************************

			public function isTripOngoing_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				if(!empty($inputArray['driverId'])){
					$driverId = $inputArray['driverId'];
				}else{
					$driverId = "";
				}
				if(!empty($inputArray['travellerId'])){
					$travellerId = $inputArray['travellerId'];
				}else{
					$travellerId = "";
				}
				$endResponse = null;
				// using driverId
				if(!empty($driverId)){
					$getListOfTripsData	= $this->Admin_model->getTripStatusUsingDriverId($driverId);
					if(!empty($getListOfTripsData)){
						$latestRecord = $getListOfTripsData[0]['tripStatus'];

						$tripInfos = array();
						$tripInfosDriverId = $getListOfTripsData[0]['driverId']??"";
						$tripInfosTripId = $getListOfTripsData[0]['tripId']??"";

						if($latestRecord=='requested'){
							$endResponse = array('isTripOnGoing'=>true,"tripStatus"=>"driverIsYetToAcceptTrip","driverId"=>$tripInfosDriverId,"tripId"=>$tripInfosTripId);
						}elseif($latestRecord=='accepted'){
							$endResponse = array('isTripOnGoing'=>true,"tripStatus"=>"driverArriving","driverId"=>$tripInfosDriverId,"tripId"=>$tripInfosTripId);
						}elseif($latestRecord=='started'){
							$endResponse = array('isTripOnGoing'=>true,"tripStatus"=>"tripStarted","driverId"=>$tripInfosDriverId,"tripId"=>$tripInfosTripId);
						}
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip status details",
							'responseData' => $endResponse
						], REST_Controller::HTTP_OK);
					}else{
						$endResponse = array('isTripOnGoing'=>false,"tripStatus"=>false);
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip status details",
							'responseData' => $endResponse
						], REST_Controller::HTTP_OK);
					}
					// using travellerId
				}elseif (!empty($travellerId)) {
					$getListOfTripsDataUsingTravellerId	= $this->Admin_model->getTripStatusUsingTravellerId($travellerId);

					if(!empty($getListOfTripsDataUsingTravellerId)){
						$latestDbRecord = $getListOfTripsDataUsingTravellerId[0]['tripStatus'];

						$tripInfo = array();
						$tripInfoDriverId = $getListOfTripsDataUsingTravellerId[0]['driverId']??"";
						$tripInfoTripId = $getListOfTripsDataUsingTravellerId[0]['tripId']??"";

						if($latestDbRecord=='requested'){
							$endResponse = array('isTripOnGoing'=>true,"tripStatus"=>"driverIsYetToAcceptTrip",
								"driverId"=>$tripInfoDriverId,"tripId"=>$tripInfoTripId);
						}elseif($latestDbRecord=='accepted'){
							$endResponse = array('isTripOnGoing'=>true,"tripStatus"=>"driverArriving","driverId"=>$tripInfoDriverId,"tripId"=>$tripInfoTripId);
						}elseif($latestDbRecord=='started'){
							$endResponse = array('isTripOnGoing'=>true,"tripStatus"=>"tripStarted","driverId"=>$tripInfoDriverId,"tripId"=>$tripInfoTripId);
						}else{
							$endResponse = array('isTripOnGoing'=>false,"tripStatus"=>false);
						}
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip status details",
							'responseData' => $endResponse
						], REST_Controller::HTTP_OK);
					}else{
						$endResponse = array('isTripOnGoing'=>false,"tripStatus"=>false);
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip status details",
							'responseData' => $endResponse
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Enter driverId or travellerId",
					], REST_Controller::HTTP_OK);
				}
			}

			// **************************************************************************************************


			public function updatePhoneNo_POST(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST)); 

				$oldPhNo = (!empty($inputArray['oldPhNo'])?$inputArray['oldPhNo']:$this->response([
					'responseMsg' => "oldPhNo is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$newPhNo = (!empty($inputArray['newPhNo'])?$inputArray['newPhNo']:$this->response([
					'responseMsg' => "newPhNo is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));

				$userData['userHashId'] = $userId;
				$checkUserExist = $this->Admin_model->checkUserExist($userData);

				if(!empty($checkUserExist)){
					$dbRegMob = $checkUserExist[0]['signInMobile'];
					if($dbRegMob!=$oldPhNo){
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "oldPhNo is Incorrect",
						], REST_Controller::HTTP_OK);
					}else{
						$userData['signInMobile'] = $newPhNo;
						$checkUserExist = $this->Admin_model->checkUserExist($userData);
						if(!empty($checkUserExist)){
							$this->response([
								'responseCode' => 400,
								'responseMsg' => "New Mobile No is already registered with another user",
							], REST_Controller::HTTP_OK);
						}else{
							$deviceData['signInMobile'] = $newPhNo;
							$updateDeviceDetails = $this->Admin_model->updateDeviceDetails($deviceData,$userId);

							//send Otp - start
							$otp = substr(str_shuffle("0123456789"),0, 4);
							$otpGenTime = time();
							$countryCode ="+91";
							$dataArr = array("userType"=>"user","countryCode"=>$countryCode,"mobile"=>$newPhNo,"otp"=>$otp,"otpGenTime"=>$otpGenTime,"hash"=>md5($newPhNo));
							$StoreToDb = $this->Admin_model->storeSendOtpData($dataArr);

							$apiUrl = "http://m.messagewall.in/api/v2/sms/send?access_token=f9ea273b45ff6a8e156b935b07605aca&to=$newPhNo&message=Yo Rides new mobile no confirmation OTP:$otp&service=T&sender=AATEXT";
							$sendOtp = file_get_contents($apiUrl);
							//send Otp - end
							if(!empty($updateDeviceDetails)){
								$this->response([
									'responseCode' => 200,
									'responseMsg' => "Mobile No updated and OTP sent successfully",
									'responseData' => array("otp"=>$otp)
								], REST_Controller::HTTP_OK);
							}else{
								$this->response([
									'responseCode' => 400,
									'responseMsg' => "Mobile No Not updated successfully! Invalid userId/phNo passed",
								], REST_Controller::HTTP_OK);
							}
						}
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "userId is invalid",
					], REST_Controller::HTTP_OK);
				}
			}

		// *********************************************************************************************

			public function verifyNewPhoneNo_post(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);
				$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
					'responseMsg' => "userId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$newPhNo = (!empty($inputArray['newPhNo'])?$inputArray['newPhNo']:$this->response([
					'responseMsg' => "newPhNo is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$otpReceived = (!empty($inputArray['otpReceived'])?$inputArray['otpReceived']:$this->response([
					'responseMsg' => "otpReceived No is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST));
				$signInMobile = $newPhNo;
				$userType = "user";
				$confirmOTPinDB=$this->Admin_model->checkOtpData($otpReceived,$signInMobile,$userType);

				if(!empty($confirmOTPinDB)){
					if(time()-($confirmOTPinDB[0]['otpGenTime']) >= 30) {
						$this->response([
							'responseMsg' => "OTP is expired",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}else{
						$this->response([
							'responseMsg' => "OTP is verified successfully",
							'responseCode' => 200,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}else{
					$this->response([
						'responseMsg' => "OTP is invalid",
						'responseCode' => 400,
					], REST_Controller::HTTP_BAD_REQUEST);
				}
			}

		//*****************************************************************
			public function getCancelledRideDetails_post(){
				$body = file_get_contents('php://input');
				$inputArray = json_decode(json_encode(json_decode($body)), true);

				$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
					'responseMsg' => "tripId is required",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST)); 

				if(!empty($inputArray['driverId'])){
					$driverId = $inputArray['driverId'];
				}else{
					$driverId = "";
				}
				if(!empty($inputArray['travellerId'])){
					$travellerId = $inputArray['travellerId'];
				}else{
					$travellerId = "";
				}

				// journey details - start
				$journeyDetails = $this->Admin_model->getJourneyDetails($tripId);
				if(!empty($journeyDetails)){
					$vehicleId = $journeyDetails[0]['vehicleId']??null;
					$getvehicleTypeIs = $this->Admin_model->getVehicleConfig($vehicleId);
				// if(!empty($driverId)){
					$vData['userHashId'] = $journeyDetails[0]['driverId'];
					$checkVehicleData = $this->Admin_model->checkVehicleData($vData);
				// }
					$journeyDetailsArr = array("journeyDetails" => array(
						"toLat"=> $journeyDetails[0]['toLatitude']??null,   
						"toLong"=> $journeyDetails[0]['toLongitude']??null,
						"fromLat"=> $journeyDetails[0]['fromLatitude']??null,   
						"fromLong"=> $journeyDetails[0]['fromLongitude']??null,   
						"fromAddress"=> $journeyDetails[0]['fromAddress']??null,   
						"toAddress"=> $journeyDetails[0]['toAddress']??null,   
						"vehicleId"=> $journeyDetails[0]['vehicleId']??null,   
						"vehicleName"=> $getvehicleTypeIs[0]['vType']??null,  
						"vehicleNo"=> $checkVehicleData[0]['vehicleRegNo']??null   
					));
				}

				// journey details - end

				// $pmId = $journeyDetails[0]['pmId'];
				// $tripPaymentMode = $this->Admin_model->getPaymentModeDetails($pmId);

				// $billingDetailsArr = array("billingDetails"=>array(
					// "finalTripAmount" => (int)$journeyDetails[0]['finalTripAmount'] ?? null,
					// "roundedOffFinalTripAmount" => (int)0,
					// "totalAmountIncludingTax" => (int)ceil($journeyDetails[0]['finalTripAmount']),
					// "paymentMode"=> $tripPaymentMode[0]['paymentMode']
				// ));

				// $journeyAndBillingArr = array_merge($journeyDetailsArr,$billingDetailsArr);
				// $tripId = $tripIdfromDB[0]['tripId'];
				$getTripDetails = $this->Admin_model->getTripDetails($tripId);
				$dbTravellerId = $getTripDetails[0]['userId'];
				$dbDriverId = $getTripDetails[0]['driverId'];

				//driver detils - start
				if(!empty($travellerId)){
					$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($dbDriverId);
					$driverImage =  base_url().'uploads/userImage/'.$driverLatestLoc[0]['userHashId'].'.png';
					$driverDetailsArr = array(
						"driverDetails"=> array(
							"driverName"=> $driverLatestLoc[0]['userName']?? null,
							"driverEmail"=> $driverLatestLoc[0]['userEmail']?? null,
							"driverImage"=> $driverImage,
							"driverMobileNo"=> $driverLatestLoc[0]['signInMobile']?? null,
							"driverId"=> $driverLatestLoc[0]['userHashId']?? null,
							"driverCurLat"=> $driverLatestLoc[0]['latitude']?? null,
							"driverCurLong"=> $driverLatestLoc[0]['longitude']?? null
						));
					$res = array_merge($journeyDetailsArr,$driverDetailsArr);
				}
				//driver detils - end
				//travellerdetails - start
				elseif(!empty($driverId)){
					$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($dbTravellerId);
					$travellerImage =  base_url().'uploads/userImage/'.$travellerLatestLoc[0]['userHashId'].'.png';
					$travellerDetailsArr = array("travellerDetails"=> array(
						"travellerName"=> $travellerLatestLoc[0]['userName']??null,
						"travellerEmail"=> $travellerLatestLoc[0]['userEmail']??null,
						"travellerImage"=> $travellerImage,
						"travellerMobileNo"=> $travellerLatestLoc[0]['signInMobile']??null,
						"travellerId"=> $travellerLatestLoc[0]['userHashId']??null,
						"travellerCurLat"=> $travellerLatestLoc[0]['latitude']??null,
						"travellerCurLong"=> $travellerLatestLoc[0]['longitude']??null
					));
				//travellerdetails - end
					$res = array_merge($journeyDetailsArr,$travellerDetailsArr);
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "driverId or travellerId is required",
					], REST_Controller::HTTP_OK);
				}
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "cancelled Trip details were obtained successfully !",
					'responseData' => $res,
				], REST_Controller::HTTP_OK);
			}
		//*****************************************************************

			



		}// end of class
		?>