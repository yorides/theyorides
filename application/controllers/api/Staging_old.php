<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';


class Staging extends REST_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database(); 
		// $this->load->library('upload');
		$this->load->model('Admin_model');
		$this->load->library(array("form_validation",'session','upload','pagination'));
		$this->load->helper(array('form', 'url','security')); 
		define('pricePerKM',10);
		//Encrypt and Decrypt Data
		define('ENCRYPTION_KEY', '__^%&Q@#$%^&*^__');
		define( 'API_ACCESS_KEY', 'AAAA0zmDieA:APA91bHTrc8R24Pb4mhgkZl7-uf_bQNKsMFvFIaSBedImyNNpDAwepunE62n5nRkttTKjDym8ErCJqcQ3NT4SCre8z1Dk4Fy38J8Y370ux3NWQPL5OzX8A62uDWLX6diM1N2clGRNHRW' );
	}


	function uploadImage($imageName,$file){
		$target_path = "./uploads/".$file."/";
		$target_path = $target_path .$imageName.".png"; 
		if(move_uploaded_file($_FILES[$file]['tmp_name'], $target_path)) {
			return 1;
		} else{
			return null;
		}
	}

	// ********************************************************************************************************


	public function CalDis($lat1, $lon1, $lat2, $lon2, $unit) {

		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}

// ********************************************************************************
	public function sendFCM($msgTitle,$getDeviceIds){

	// //Google Fire base Api - Start
		$msg = array (
			'body' 	=> 'Yo Rides',
			'title'		=> $msgTitle,
		);
		$fields = array (
			'to'        => '/topics/General',
			'notification'    => $msg,
			"priority" => "high",
			"sound" =>"default",
			"vibrate" =>"default"
		);
		$headers = array (
			'Authorization: key='.API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		curl_close($ch);
		return json_encode($result);


	// 	$fields = array ('registration_ids' => array ($getDeviceIds), 'data' => array ("message" => $msgTitle ) );
 // //header includes Content type and api key
	// 	$headers = array('Content-Type:application/json', 'Authorization:key='.API_ACCESS_KEY );
	// 	$ch = curl_init();
	// 	curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	// 	curl_setopt($ch, CURLOPT_POST, true);
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	// 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	// 	$result = curl_exec($ch);
	// 	if ($result === FALSE) {
	// 		die('FCM Send Error: ' . curl_error($ch));
	// 	}
	// 	curl_close($ch);
	// 	return $result;
	// 				//Google Fire base Api - End




		$url = "https://fcm.googleapis.com/fcm/send";
		$token = $getDeviceIds;
		$serverKey = API_ACCESS_KEY;
		$title = $msgTitle;
		$body = $msgTitle;
		$notification = array('title' =>$title , 'body' => $body, 'sound' => 'default', 'badge' => '1');
		$arrayToSend = array('to' => json_encode($token), 'notification' => $notification,'priority'=>'high');
		$json = json_encode($arrayToSend);
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Authorization: key='. $serverKey;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
    //Send the request
		$response = curl_exec($ch);
    //Close request
		if ($response === FALSE) {
			die('FCM Send Error: ' . curl_error($ch));
		}
		curl_close($ch);





	}



	// public function sendFCM($arr) {

	// 	$arr = array(
	// 		'registration_ids' => $androidTokens,
	// 		'notification' => array( 'title' => $notificationTitle, 'body' => $notificationBody),
	// 		'data' => array( 'title' => $notificationTitle, 'body' => $notificationBody)
	// 	);


 //    //Google Firebase messaging FCM-API url
	// 	$url = 'https://fcm.googleapis.com/fcm/send';
	// 	$fields = (array) $arr;
	// 	define("GOOGLE_API_KEY","XXXXXXXXXXXXXXXXXXXXX");
	// 	$headers = array(
	// 		'Authorization: key=' . GOOGLE_API_KEY,
	// 		'Content-Type: application/json'
	// 	);
	// 	$ch = curl_init();
	// 	curl_setopt($ch, CURLOPT_URL, $url);
	// 	curl_setopt($ch, CURLOPT_POST, true);
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);   
	// 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	// 	$result = curl_exec($ch);               
	// 	if ($result === FALSE) {
	// 		die('Curl failed: ' . curl_error($ch));
	// 	}
	// 	curl_close($ch);
	// 	return $result;
	// }





	// *************************************************************************************************************************
	// CONFIRM OTP API
	public function confirmOtp_post(){
		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);
		$userType = (!empty($inputArray['userType'])?$inputArray['userType']:$this->response([
			'responseMsg' => "userType is required (Either T or D)",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$otpReceived = (!empty($inputArray['otpReceived'])?$inputArray['otpReceived']:$this->response([
			'responseMsg' => "otpReceived is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$signInMobile = (!empty($inputArray['signInMobile'])?$inputArray['signInMobile']:$this->response([
			'responseMsg' => "signInMobile No is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$confirmOTPinDB=$this->Admin_model->checkOtpData($otpReceived,$signInMobile,$userType);
		if(!empty($confirmOTPinDB)){
			if(time()-($confirmOTPinDB[0]['otpGenTime']) >= 30) {
				$this->response([
					'responseMsg' => "OTP is expired",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST);
			}else{
				$userData['userType'] = (!empty($userType)?$userType:"");
				$userData['signInMobile'] = (!empty($signInMobile)?$signInMobile:"");
				$userData['allowSignIn'] = 1;
				$userDetails['signInMobile'] = $signInMobile;
				$checkUserExist = $this->Admin_model->checkUserData($userDetails);
				if(!empty($checkUserExist)){
					$signInMobile = $userDetails['signInMobile'];
					$detailsData['allowSignIn'] = 1;
					$updateDeviceDetails = $this->Admin_model->updateUserDetails($detailsData,$signInMobile);
					$this->response([
						'responseMsg' => "user already exist",
						'responseCode' => 200,
						'responseData' => array(
							"isUserExists"=>true,
							"userId" => $checkUserExist[0]['userHashId'],
							"userType" => $checkUserExist[0]['userType']
						),
					], REST_Controller::HTTP_BAD_REQUEST);
				}else{
					$dbUserId = $this->Admin_model->storeUserData($userData);
					if(!empty($dbUserId)){
						$updateUserHash = $this->Admin_model->updateHashId($dbUserId);
						$this->response([
							'responseMsg' => "OTP is verified",
							'responseCode' => 200,
							'responseData' => array('userType'=>$userType,"userId" =>md5($dbUserId)),
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseMsg' => "OTP is invalid",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}
			}
		}else{
			$this->response([
				'responseMsg' => "OTP is invalid",
				'responseCode' => 400,
			], REST_Controller::HTTP_BAD_REQUEST);
		}
	}
// *******************************************************************************************************
	public function sendOtp_post(){
		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);
		$userType = (!empty($inputArray['userType'])?$inputArray['userType']:$this->response([
			'responseMsg' => "userType is required (Either T or D)",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$userTypeArr = array("T","D");
		$countryCode = (!empty($inputArray['countryCode'])?$inputArray['countryCode']:$this->response([
			'responseMsg' => "countryCode is required (eg +91)",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$signInMobile = (!empty($inputArray['signInMobile'])?$inputArray['signInMobile']:$this->response([
			'responseMsg' => "signInMobile No is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$userDetails['signInMobile'] = $signInMobile;
		// $userDetails['allowSignIn'] = 1;
		// $userDetails['userType'] = $userType;
		$checkUserExist = $this->Admin_model->checkUserData($userDetails);
		if(!empty($checkUserExist)){
			if(($checkUserExist[0]['allowSignIn']==1)and($checkUserExist[0]['userType']==$userType)){
				$otp = substr(str_shuffle("0123456789"),0, 4);
				$otpGenTime = time();
				$dataArr = array("userType"=> $userType,"countryCode"=>$countryCode,"mobile"=>$signInMobile,"otp"=>$otp,"otpGenTime"=>$otpGenTime,"hash"=>md5($signInMobile));
				$StoreToDb = $this->Admin_model->storeSendOtpData($dataArr);
				$apiUrl = "http://m.messagewall.in/api/v2/sms/send?access_token=f9ea273b45ff6a8e156b935b07605aca&to=$signInMobile&message=Yo Rides OTP:$otp&service=T&sender=AATEXT";
				$sendOtp = file_get_contents($apiUrl);
		// $sendOtp = true;
				if(!empty($sendOtp)){
					$this->response([
						"responseMsg" => "OTP has been sent",
						"responseCode" => 200,
						"otpResponseData" => array("otp"=> $otp)
					], REST_Controller::HTTP_OK);
				}else{
					$this->response([
						'responseMsg' => "OTP sending is failure",
						'responseCode' => 400,
					], REST_Controller::HTTP_BAD_REQUEST);
				}
			}elseif(($checkUserExist[0]['allowSignIn']==0)and($checkUserExist[0]['userType']==$userType)){
				$otp = substr(str_shuffle("0123456789"),0, 4);
				$otpGenTime = time();
				$dataArr = array("userType"=> $userType,"countryCode"=>$countryCode,"mobile"=>$signInMobile,"otp"=>$otp,"otpGenTime"=>$otpGenTime,"hash"=>md5($signInMobile));
				$StoreToDb = $this->Admin_model->storeSendOtpData($dataArr);
				$apiUrl = "http://m.messagewall.in/api/v2/sms/send?access_token=f9ea273b45ff6a8e156b935b07605aca&to=$signInMobile&message=Yo Rides OTP:$otp&service=T&sender=AATEXT";
				$sendOtp = file_get_contents($apiUrl);
		// $sendOtp = true;
				if(!empty($sendOtp)){
					$this->response([
						"responseMsg" => "OTP has been sent",
						"responseCode" => 200,
						"otpResponseData" => array("otp"=> $otp)
					], REST_Controller::HTTP_OK);
				}else{
					$this->response([
						'responseMsg' => "OTP sending is failure",
						'responseCode' => 400,
					], REST_Controller::HTTP_BAD_REQUEST);
				}
			}else{
				$userModeFromDb = ($checkUserExist[0]['userType']=="D")?"driver":"traveller";
				$mess = "The same number is already used in ".$userModeFromDb;
				$this->response([
					'responseMsg' => $mess,
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}else{
			$otp = substr(str_shuffle("0123456789"),0, 4);
			$otpGenTime = time();
			$dataArr = array("userType"=> $userType,"countryCode"=>$countryCode,"mobile"=>$signInMobile,"otp"=>$otp,"otpGenTime"=>$otpGenTime,"hash"=>md5($signInMobile));
			$StoreToDb = $this->Admin_model->storeSendOtpData($dataArr);
			$apiUrl = "http://m.messagewall.in/api/v2/sms/send?access_token=f9ea273b45ff6a8e156b935b07605aca&to=$signInMobile&message=Yo Rides OTP:$otp&service=T&sender=AATEXT";
			$sendOtp = file_get_contents($apiUrl);
		// $sendOtp = true;
			if(!empty($sendOtp)){
				$this->response([
					"responseMsg" => "OTP has been sent",
					"responseCode" => 200,
					"otpResponseData" => array("otp"=> $otp)
				], REST_Controller::HTTP_OK);
			}else{
				$this->response([
					'responseMsg' => "OTP sending is failure",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}
	}
// *********************************************************************************************************************
	public function vehicleReg_post(){

		$userId = (!empty($_POST['userId'])?$_POST['userId']:$this->response([
			'responseMsg' => "userId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$signInMobile = (!empty($_POST['signInMobile'])?$_POST['signInMobile']:$this->response([
			'responseMsg' => "signInMobile No is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$vehicleRegNo = (!empty($_POST['vehicleRegNo'])?$_POST['vehicleRegNo']:$this->response([
			'responseMsg' => "vehicleRegNo is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$licNo = (!empty($_POST['licNo'])?$_POST['licNo']:$this->response([
			'responseMsg' => "licNo is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$licCopy = (!empty($_FILES['licCopy'])?$_FILES['licCopy']:$this->response([
			'responseMsg' => "licCopy is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$tradeLicCopy = (!empty($_FILES['tradeLicCopy'])?$_FILES['tradeLicCopy']:$this->response([
			'responseMsg' => "tradeLicCopy is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$routePermitCopy = (!empty($_FILES['routePermitCopy'])?$_FILES['routePermitCopy']:$this->response([
			'responseMsg' => "routePermitCopy is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$vehiclePhotoFront = (!empty($_FILES['vehiclePhotoFront'])?$_FILES['vehiclePhotoFront']:$this->response([
			'responseMsg' => "vehiclePhotoFront is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$vehiclePhotoBack = (!empty($_FILES['vehiclePhotoBack'])?$_FILES['vehiclePhotoBack']:$this->response([
			'responseMsg' => "vehiclePhotoBack is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$vehicleId = (!empty($_POST['vehicleId'])?$_POST['vehicleId']:$this->response([
			'responseMsg' => "vehicleId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$regCopyFront = (!empty($_FILES['regCopyFront'])?$_FILES['regCopyFront']:$this->response([
			'responseMsg' => "regCopyFront is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$regCopyBack = (!empty($_FILES['regCopyBack'])?$_FILES['regCopyBack']:$this->response([
			'responseMsg' => "regCopyBack is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$userDetails['signInMobile'] = $signInMobile;
		$userDetails['userHashId'] = $userId;
		$checkUserExist = $this->Admin_model->checkUserData($userDetails);

		if(!empty($checkUserExist)){

			$userMode = $checkUserExist[0]['userType'];
			if($userMode=='D'){


				$userData["signInMobile"] =  $_POST['signInMobile'];
				$userData["userHashId"] =  ($_POST['userId']);
				$userData["vehicleRegNo"] =  $_POST['vehicleRegNo'];
				$userData["licNo"] =  $_POST['licNo'];
				$userData["vehicleId"] =  $_POST['vehicleId'];

			//regCopyFront
				$imageName = $_POST['userId'];
				$file = 'regCopyFront';
				$regCopyFrontUpload = $this->uploadImage($imageName,$file);

			//regCopyBack
				$imageName = $_POST['userId'];
				$file = 'regCopyBack';
				$regCopyBackUpload = $this->uploadImage($imageName,$file);

			//lic Copy
				$imageName = $_POST['userId'];
				$file = 'licCopy';
				$licCopyUpload = $this->uploadImage($imageName,$file);

			//tradeLicCopy
				$imageName = $_POST['userId'];
				$file = 'tradeLicCopy';
				$tradeLicUpload = $this->uploadImage($imageName,$file);

			//routePermitCopy
				$imageName = $_POST['userId'];
				$file = 'routePermitCopy';
				$routePermitUpload = $this->uploadImage($imageName,$file);

			//vehiclePhotoFront
				$imageName = $_POST['userId'];
				$file = 'vehiclePhotoFront';
				$vehiclePhotoFrontUpload = $this->uploadImage($imageName,$file);

			//vehiclePhotoBack
				$imageName = $_POST['userId'];
				$file = 'vehiclePhotoBack';
				$vehiclePhotoBackUpload = $this->uploadImage($imageName,$file);

				$vData['signInMobile'] = $userData["signInMobile"];
				$checkVehicleData = $this->Admin_model->checkVehicleData($vData);

				$vehicleTypeIs = $this->Admin_model->getVehicleConfig($vehicleId);

				if(!empty($checkVehicleData)){
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Vehicle Reg data is already updated",
					// 'responseData' => array('vehicleid'=>$vehicleId),
					], REST_Controller::HTTP_BAD_REQUEST);
				}else{
					$storeVehicleData = $this->Admin_model->storeVehicleData($userData);
					if(!empty($storeVehicleData)){

						$storedData['userId'] = $userId;
						$storedData['signInMobile'] = $signInMobile;
						$storedData['vehicleRegNo'] = $vehicleRegNo;
						$storedData['licNo'] = $licNo;
						$storedData['vehicleId'] = $vehicleId;
						$storedData['vehicleType'] = $vehicleTypeIs[0]['vType'] ?? null;
						$storedData['licCopy'] = base_url().'uploads/licCopy/'.$userId.'.png';
						$storedData['tradeLicCopy'] = base_url().'uploads/tradeLicCopy/'.$userId.'.png';
						$storedData['routePermitCopy'] = base_url().'uploads/routePermitCopy/'.$userId.'.png';
						$storedData['vehiclePhotoFront'] = base_url().'uploads/vehiclePhotoFront/'.$userId.'.png';
						$storedData['vehiclePhotoBack'] = base_url().'uploads/vehiclePhotoBack/'.$userId.'.png';
						$storedData['regCopyFront'] = base_url().'uploads/regCopyFront/'.$userId.'.png';
						$storedData['regCopyBack'] = base_url().'uploads/regCopyBack/'.$userId.'.png';

						$this->response([
							'responseMsg' => "Vehicle Reg data is updated",
							'responseCode' => 200,
							'responseData' => array($storedData)
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseMsg' => "Vehicle Reg updating is failed",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}
			}else{
				$this->response([
					'responseMsg' => "The same number is already used in traveller",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST);
			}

		}else{
			$this->response([
				'responseMsg' => "signInMobile is invalid or Mobile no is incorrect",
				'responseCode' => 400,
			], REST_Controller::HTTP_BAD_REQUEST);
		}
	}


// *******************************************************************************************************
	public function paymentModeList_GET(){
		$getPaymentModeList = $this->Admin_model->getPaymentModeList();
		$this->response([
			'responseCode' => 200,
			'responseMsg' => "Payment Mode List Details",
			'responseData' => $getPaymentModeList,
		], REST_Controller::HTTP_OK);
	}

	// *********************************************************************************************************************
	public function vehicleList_GET(){
		$getVehicleList = $this->Admin_model->getVehicleList();
		$this->response([
			'responseCode' => 200,
			'responseMsg' => "Vehicle List Details",
			'responseData' => $getVehicleList,
		], REST_Controller::HTTP_OK);
	}
// *******************************************************************************************************
		// User Existence
	public function fetchUserInfo_post(){
		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);
		$userType = (!empty($inputArray['userType'])?$inputArray['userType']:$this->response([
			'responseMsg' => "userType is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
			'responseMsg' => "userId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$deviceID = (!empty($inputArray['deviceID'])?$inputArray['deviceID']:$this->response([
			'responseMsg' => "deviceID is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$deviceIp = (!empty($inputArray['deviceIp'])?$inputArray['deviceIp']:$this->response([
			'responseMsg' => "deviceIp is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$deviceMac = (!empty($inputArray['deviceMac'])?$inputArray['deviceMac']:$this->response([
			'responseMsg' => "deviceMac is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$deviceOs = (!empty($inputArray['deviceOs'])?$inputArray['deviceOs']:$this->response([
			'responseMsg' => "deviceOs is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$fcmToken = (!empty($inputArray['fcmToken'])?$inputArray['fcmToken']:$this->response([
			'responseMsg' => "fcmToken is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$userData['userType'] = $userType;
		$userData['userHashId'] = $userId;
		$userData['allowSignIn'] = 1;
		$checkUserExist = $this->Admin_model->checkUserExist($userData);
		if(!empty($checkUserExist)){
			$deviceData['deviceId'] = $deviceID;
			$deviceData['deviceIp'] = $deviceIp;
			$deviceData['deviceMac'] = $deviceMac;
			$deviceData['deviceOs'] = $deviceOs;
			$deviceData['fcmToken'] = $fcmToken;
			$deviceData['allowSignIn'] = 1;
			$updateDeviceDetails = $this->Admin_model->updateDeviceDetails($deviceData,$userId);
			if(!empty($updateDeviceDetails)){
				$this->response([
					'responseMsg' => "user exist",
					"responseData"=> array(
						"signInMobile"=>$checkUserExist[0]['signInMobile'],
						"userEmail"=>$checkUserExist[0]['userEmail'],
						"userName"=>$checkUserExist[0]['userName'],
						"deviceID"=>$checkUserExist[0]['deviceID'],
						"deviceMac"=>$checkUserExist[0]['deviceMac'],
						"deviceIp"=>$checkUserExist[0]['deviceIp'],
						"deviceOs"=>$checkUserExist[0]['deviceOs'],
						"fcmToken"=>$checkUserExist[0]['fcmToken']),
					'responseCode' => 200,
				], REST_Controller::HTTP_OK);
			}else{
				$this->response([
					'responseMsg' => "user not exist",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}else{
			$this->response([
				'responseMsg' => "user is signed out",
				'responseCode' => 400,
			], REST_Controller::HTTP_BAD_REQUEST);
		}
	}

// *************************************************************************************

	public function cutomerSupport_get(){
		$this->response([
			'responseCode' => 200,
			'responseMsg' => "Customer Support Details",
			'responseData' => array("mobile"=>"9748703099"),
		], REST_Controller::HTTP_OK);

	}

// *************************************************************************************

	public function updateProfileInfo_post(){

		$userId = (!empty($_POST['userId'])?$_POST['userId']:$this->response([
			'responseMsg' => "userId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$userName = (!empty($_POST['userName'])?$_POST['userName']:$this->response([
			'responseMsg' => "userName is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$userEmail = (!empty($_POST['userEmail'])?$_POST['userEmail']:$this->response([
			'responseMsg' => "userEmail is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		// $refferalCode = $_POST['refferalCode'];
		// $userImage = $_FILES['userImage'];
		if(!empty($_FILES['userImage'])){
			$imageName = $userId;
			$file = 'userImage';
			$profileImageUpload = $this->uploadImage($imageName,$file);
		}
		$userProfile['userEmail'] = $userEmail;
		$userProfile['userName'] = $userName;
		if(!empty($_POST['refferalCode'])){
			$userProfile['refferalCode'] = $refferalCode = $_POST['refferalCode'];
		}
		$userData['userHashId'] = $userId;
		$checkUserExist = $this->Admin_model->checkUserData($userData);
		if(!empty($checkUserExist)){
			$addProfileData = $this->Admin_model->addProfileData($userProfile,$userId);
			if(!empty($addProfileData)){
				$this->response([
					'responseMsg' => "User Profile Updated",
					'responseCode' => 200,
				], REST_Controller::HTTP_OK);
			}else{
				$this->response([
					'responseMsg' => "User Profile Not Updated",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}else{
			$this->response([
				'responseMsg' => "User Not Found",
				'responseCode' => 400,
			], REST_Controller::HTTP_OK);
		}

	}

// ***************************************************************************************************

	public function updateLocation_post(){

		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);
		$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
			'responseMsg' => "userId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$latitude = (!empty($inputArray['latitude'])?$inputArray['latitude']:$this->response([
			'responseMsg' => "latitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$longitude = (!empty($inputArray['longitude'])?$inputArray['longitude']:$this->response([
			'responseMsg' => "longitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		if(!empty($inputArray['userAddress'])){
			$userAddress = $inputArray['userAddress'];
			$userLocationData['userAddress'] = $userAddress;
		}
		$userLocationData['latitude'] = $latitude;
		$userLocationData['longitude'] = $longitude;
		$userData['userHashId'] = $userId;
		$checkUserExist = $this->Admin_model->checkUserData($userData);
		if(!empty($checkUserExist)){
			$locationUpdated = $this->Admin_model->updateUserLocation($userLocationData,$userId);
			if(!empty($locationUpdated)){
				$this->response([
					'responseMsg' => "User location Updated",
					'responseCode' => 200,
				], REST_Controller::HTTP_OK);
			}else{
				$this->response([
					'responseMsg' => "User location Not Updated",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}else{
			$this->response([
				'responseMsg' => "User Not Found",
				'responseCode' => 400,
			], REST_Controller::HTTP_OK);
		}

	}

// *********************************************************************************************************

	public function travellerLookingForTrip_POST(){

		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);

		$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
			'responseMsg' => "userId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$fromLatitude = (!empty($inputArray['fromLatitude'])?$inputArray['fromLatitude']:$this->response([
			'responseMsg' => "fromLatitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$fromLongitude = (!empty($inputArray['fromLongitude'])?$inputArray['fromLongitude']:$this->response([
			'responseMsg' => "fromLongitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$toLatitude = (!empty($inputArray['toLatitude'])?$inputArray['toLatitude']:$this->response([
			'responseMsg' => "toLatitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$toLongitude = (!empty($inputArray['toLongitude'])?$inputArray['toLongitude']:$this->response([
			'responseMsg' => "toLongitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		if(!empty($inputArray['fromAddress'])){
			$fromAddress = $inputArray['fromAddress'];
		}else{
			$fromAddress = "";
		}
		if(!empty($inputArray['toAddress'])){
			$toAddress = $inputArray['toAddress'];
		}else{
			$toAddress = "";
		}
		$userData['userHashId'] = $userId;
		$checkUserExist = $this->Admin_model->checkUserData($userData);
		if(!empty($checkUserExist)){
			$distInKM= $this->CalDis($fromLatitude,$fromLongitude,$toLatitude,$toLongitude,"K");
			if($distInKM>0){
				$distInKM = number_format($distInKM,'2');
				$getActiveDriversList = $this->Admin_model->getActiveDriversList();
				$availableDriversList = (array_column($getActiveDriversList, "vehicleId"));
				if(!empty($availableDriversList)){
					$getFilteredVehicleList = $this->Admin_model->getFilteredVehicleList($availableDriversList);
				// $selectedVehicleDriverList = $this->Admin_model->selectedVehicleDriverList($vehicleId); 
				// if(!empty($selectedVehicleDriverList)){
				// 	foreach ($selectedVehicleDriverList as $kk  => $vv) {
				// 		$userIdForDriversList[] = trim($vv['userHashId']);
				// 	}
				// $getDriversList = $this->Admin_model->getDriversListUsingUserId($userIdForDriversList);
					foreach ($getFilteredVehicleList as $key =>$value) {
						$vehicles[$key]['estimatedAmount']	= (int)($value['pricePerKM']*$distInKM*$value['surCharge']);	
						$vehicles[$key]['vType']	= ucwords(strtolower($value['vType']));				
						$vehicles[$key]['vehicleId']	= $value['vehicleId'];				
						$vehicles[$key]['distInKM']	= $distInKM;				
						$vehicles[$key]['surCharge']	= (int)$value['surCharge'];
						$vehicles[$key]['pricePerKM']	= (int)$value['pricePerKM'];	
					}
					sort($vehicles);
					$this->response([
						'responseCode' => 200,
						'responseMsg' => "Trip details for Confirmation",
						"responseData" => array(
							"userId" => $checkUserExist[0]['userHashId'],
							"fromLatitude" => $fromLatitude,
							"fromLongitude" => $fromLongitude,
							"toLatitude" => $toLatitude,
							"toLongitude" => $toLongitude,
							"toAddress" => $toAddress,
							"fromAddress" => $fromAddress,
							"estimatedDetails" => $vehicles,
						)
					], REST_Controller::HTTP_OK);
				}
			}else{
				$this->response([
					'responseMsg' => "From location and To location should not be same",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}else{
			$this->response([
				'responseMsg' => "User Not Found",
				'responseCode' => 400,
			], REST_Controller::HTTP_OK);
		}

	}

// ************************************************************************************************************

	public function travellerConfirmTrip_POST(){

		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);

		$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
			'responseMsg' => "userId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$vehicleId = (!empty($inputArray['vehicleId'])?$inputArray['vehicleId']:$this->response([
			'responseMsg' => "vehicleId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$fromLatitude = (!empty($inputArray['fromLatitude'])?$inputArray['fromLatitude']:$this->response([
			'responseMsg' => "fromLatitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$fromLongitude = (!empty($inputArray['fromLongitude'])?$inputArray['fromLongitude']:$this->response([
			'responseMsg' => "fromLongitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$toLatitude = (!empty($inputArray['toLatitude'])?$inputArray['toLatitude']:$this->response([
			'responseMsg' => "toLatitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$toLongitude = (!empty($inputArray['toLongitude'])?$inputArray['toLongitude']:$this->response([
			'responseMsg' => "toLongitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$pmId = (!empty($inputArray['pmId'])?$inputArray['pmId']:$this->response([
			'responseMsg' => "pmId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$userData['userHashId'] = $userId;
		$checkUserExist = $this->Admin_model->checkUserData($userData);
		if(!empty($checkUserExist)){
			$distInKM= $this->CalDis($fromLatitude,$fromLongitude,$toLatitude,$toLongitude,"K");
			if($distInKM>0){
				$getVehicleList = $this->Admin_model->getVehicleList();
				foreach ($getVehicleList as $key =>$value) {
					$vehicles[$key]['vType']	= $value['vType'];				
					$vehicles[$key]['vehicleId']	= $value['vehicleId'];				
					$vehicles[$key]['distInKM']	= number_format($distInKM,'2');				
					$vehicles[$key]['surCharge']	= (int)$value['surCharge'];
					$vehicles[$key]['pricePerKM']	= (int)$value['pricePerKM'];	
					$vehicles[$key]['estimatedAmount']=number_format($value['pricePerKM']*$distInKM*$value['surCharge']);	
				}
				$selectedVehicleDriverList = $this->Admin_model->selectedVehicleDriverList($vehicleId); 
				if(!empty($selectedVehicleDriverList)){
					foreach ($selectedVehicleDriverList as $kk  => $vv) {
						$listOfDriversId[] = trim($vv['userHashId']);
					}
					$getDriversList = $this->Admin_model->getDriversListUsingUserId($listOfDriversId);
					if(!empty($getDriversList)){
						$getDeviceIds = array_column($this->Admin_model->getFCMIds($listOfDriversId), 'deviceID');
						$msgTitle = "A Traveller is requesting for Trip !";
						$sendFCM = $this->sendFCM($msgTitle,$getDeviceIds);
						$travellerTripReqData['userId'] = $userId;
						$travellerTripReqData['vehicleId'] = $vehicleId;
						$travellerTripReqData['fromLatitude'] = $fromLatitude;
						$travellerTripReqData['fromLongitude'] = $fromLongitude;
						$travellerTripReqData['toLatitude'] = $toLatitude;
						$travellerTripReqData['toLongitude'] = $toLongitude;
						$travellerTripReqData['tripReqTime'] = time();
						$travellerTripReqData['pmId'] = $pmId;
						$travellerTripReqData['tripReqId'] = md5(time());
						$storeTravellerReqData = $this->Admin_model->storeTravellerReqData($travellerTripReqData);
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip request sent to drivers",
							'responseData' => array('tripReqId' => $travellerTripReqData['tripReqId']),
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseMsg' => "No vehicle found ! Try Again",
							'responseCode' => 400,
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}else{
					$this->response([
						'responseMsg' => "No vehicle found ! Try Again",
						'responseCode' => 400,
					], REST_Controller::HTTP_BAD_REQUEST);
				}
			}else{
				$this->response([
					'responseMsg' => "From location and To location should not be same",
					'responseCode' => 400,
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}else{
			$this->response([
				'responseMsg' => "User Not Found",
				'responseCode' => 400,
			], REST_Controller::HTTP_OK);
		}
	}

// *****************************************************************************************************************
		//accepted
		//started
		//cancelled
		//completed
		//rejected

	public function acceptTrip_POST(){
		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);
		$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
			'responseMsg' => "travellerId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
			'responseMsg' => "driverId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$tripOtp = substr(str_shuffle("0123456789"),0, 4);
		$tripContent['travellerId'] = $travellerId; 
		$tripContent['driverId'] = $driverId;
		$tripContent['tripOtp'] = $tripOtp;
		$tripContent['tripStatus'] = 'accepted';

		$userDetails['userHashId'] = $travellerId;
		$signInMobile = $this->Admin_model->checkUserData($userDetails)[0]['signInMobile'];
		$apiUrl = "http://m.messagewall.in/api/v2/sms/send?access_token=f9ea273b45ff6a8e156b935b07605aca&to=$signInMobile&message=Yo Rides Trip OTP:$tripOtp&service=T&sender=AATEXT";
		$sendOtp = file_get_contents($apiUrl);

		// $checkTripAcceptedOrNot = $this->Admin_model->checkTripAcceptedOrNot($driverId,$travellerId);
		$dbGenTripId = $this->Admin_model->createTrip($tripContent);
		if(!empty($dbGenTripId)){
			$updateTripHash = $this->Admin_model->updateTripHashId($dbGenTripId);
			if(!empty($updateTripHash)){
				$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($driverId);
				$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);
				if(empty($driverLatestLoc)||empty($travellerLatestLoc)){
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "trip cant be accepted - invalid driverId / travellerId passed",
					], REST_Controller::HTTP_OK);
				}else{
					$TripDetails = array(
						"tripId" => $updateTripHash,
						"tripOTP"=> $tripOtp,
						"driverDetails"=> $driverLatestLoc,
						"travellerDetails"=> $travellerLatestLoc
					);
					$this->response([
						'responseCode' => 200,
						'responseMsg' => "Trip acceptance Details",
						'responseData' => $TripDetails
					], REST_Controller::HTTP_OK);
				}
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "trip cant be accepted - invalid parameters passed",
			], REST_Controller::HTTP_OK);
		}
	}
// ***************************************************************************************************************
	public function getDriverInfo_POST(){
		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);

		$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
			'responseMsg' => "travellerId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
			'responseMsg' => "tripId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$getDriverInfoIfTripIsAssigned = $this->Admin_model->getDriverInfoForTrip($tripId,$travellerId);
		if(!empty($getDriverInfoIfTripIsAssigned)){
			$driverId = $getDriverInfoIfTripIsAssigned[0]['driverId'];
			$driverLatestDetails = $this->Admin_model->getDriverLatestLoc($driverId);
			if(!empty($driverLatestDetails)){
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "Driver details Found",
					'responseData' => $driverLatestDetails,
				], REST_Controller::HTTP_OK);
			}else{
				$this->response([
					'responseCode' => 400,
					'responseMsg' => "Driver details not Found",
				], REST_Controller::HTTP_OK);
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "No Driver Assigned for this trip",
			], REST_Controller::HTTP_OK);
		}
	}
// ******************************************************************************************************************
	public function getTravellerInfo_POST(){
		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);

		$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
			'responseMsg' => "driverId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
			'responseMsg' => "tripId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$getTravellerInfoIfTripIsAssigned = $this->Admin_model->getTravellerInfoForTrip($tripId,$driverId);

		if(!empty($getTravellerInfoIfTripIsAssigned)){
			$travellerId = $getTravellerInfoIfTripIsAssigned[0]['travellerId'];
			$travellerLatestDetails = $this->Admin_model->getTravellerLatestLoc($travellerId);
			if(!empty($travellerLatestDetails)){
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "Traveller details Found",
					'responseData' => $travellerLatestDetails,
				], REST_Controller::HTTP_OK);
			}else{
				$this->response([
					'responseCode' => 400,
					'responseMsg' => "Traveller details not Found",
				], REST_Controller::HTTP_OK);
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "Traveller doesn't have any trip",
			], REST_Controller::HTTP_OK);
		}
	}

// ******************************************************************************************************************

	public function rejectTrip_POST(){
		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);
		$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
			'responseMsg' => "travellerId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
			'responseMsg' => "driverId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$tripContent['travellerId'] = $travellerId; 
		$tripContent['driverId'] = $driverId;
		$tripContent['tripStatus'] = 'rejected';
		// $checkTripAcceptedOrNot = $this->Admin_model->checkTripAcceptedOrNot($driverId,$travellerId);
		$dbGenTripId = $this->Admin_model->createTrip($tripContent);
		if(!empty($dbGenTripId)){
			$updateTripHash = $this->Admin_model->updateTripHashId($dbGenTripId);
			if(!empty($updateTripHash)){
				$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($driverId);
				$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);
				if(empty($driverLatestLoc)||empty($travellerLatestLoc)){
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "trip cant be rejected - invalid driverId / travellerId passed",
					], REST_Controller::HTTP_OK);
				}else{
					$this->response([
						'responseCode' => 200,
						'responseMsg' => "Trip is rejected successfully",
					], REST_Controller::HTTP_OK);
				}
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "Trip cant be rejected - invalid parameters passed",
			], REST_Controller::HTTP_OK);
		}
	}
// ****************************************************************************************************************

// startTrip(D) - (travellerId,driverId,tripId,TripOTP,driverPickUp lat/long) [orderId in FCM]

	public function startTrip_POST(){

		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);

		$tripOtp = (!empty($inputArray['tripOtp'])?$inputArray['tripOtp']:$this->response([
			'responseMsg' => "tripOtp is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
			'responseMsg' => "tripId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$driverPickupLatitude = (!empty($inputArray['driverPickupLatitude'])?$inputArray['driverPickupLatitude']:$this->response([
			'responseMsg' => "driverPickupLatitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$driverPickupLongitude = (!empty($inputArray['driverPickupLongitude'])?$inputArray['driverPickupLongitude']:$this->response([
			'responseMsg' => "driverPickupLongitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$getTripDetails = $this->Admin_model->getTripDetails($tripId);
		if(!empty($getTripDetails)){
			if($getTripDetails[0]['tripStatus']=='rejected'){
				$this->response([
					'responseCode' => 400,
					'responseMsg' => "Trip is rejected already",
				], REST_Controller::HTTP_OK);
			}else{
				if($getTripDetails[0]['tripOtp']==$tripOtp){
					$driverTripLoc['driverFromLongitude'] = $driverPickupLongitude;
					$driverTripLoc['driverFromLatitude'] = $driverPickupLatitude;
					$driverTripLoc['tripStatus'] = 'started';
					$driverTripLoc['tripPickUpTime'] = time();
					$updateTripDetails = $this->Admin_model->updateTrip($tripId,$driverTripLoc);
					if(!empty($updateTripDetails)){
						$this->response([
							'responseCode' => 200,
							'responseMsg' => "Trip is started",
						], REST_Controller::HTTP_OK);
					}else{
						$this->response([
							'responseCode' => 400,
							'responseMsg' => "Trip cant be started - Invalid parameters passed",
						], REST_Controller::HTTP_OK);
					}
				}else{
					$this->response([
						'responseCode' => 400,
						'responseMsg' => "Trip OTP is invalid or incorrect",
					], REST_Controller::HTTP_OK);
				}
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "Trip id is invalid or incorrect",
			], REST_Controller::HTTP_OK);
		}
	}

// ****************************************************************************
	public function endTrip_POST(){

		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);

		$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
			'responseMsg' => "tripId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$driverDropLatitude = (!empty($inputArray['driverDropLatitude'])?$inputArray['driverDropLatitude']:$this->response([
			'responseMsg' => "driverDropLatitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$driverDropLongitude = (!empty($inputArray['driverDropLongitude'])?$inputArray['driverDropLongitude']:$this->response([
			'responseMsg' => "driverDropLongitude is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$getTripDetails = $this->Admin_model->getTripDetails($tripId);

		if(!empty($getTripDetails)){
			if($getTripDetails[0]['tripStatus']=='rejected'){
				$this->response([
					'responseCode' => 400,
					'responseMsg' => "Trip is rejected already",
				], REST_Controller::HTTP_OK);
			}elseif($getTripDetails[0]['tripStatus']=='started'){
				$fromLatitude = $getTripDetails[0]['driverFromLatitude'];
				$fromLongitude = $getTripDetails[0]['driverFromLongitude'];
				$toLatitude = $driverDropLatitude;
				$toLongitude = $driverDropLongitude;
				$distInKM =  $this->CalDis($fromLatitude,$fromLongitude,$toLatitude,$toLongitude,"K");
				$vehiclePricPerKM = $this->Admin_model->getVehicleDetailsForAmountCalc($tripId);
				$driverTripLoc['driverDropLatitude'] = $driverDropLatitude;
				$driverTripLoc['driverDropLongitude'] = $driverDropLongitude;
				$driverTripLoc['tripStatus'] = 'completed';
				$driverTripLoc['finalTripAmount'] = $vehiclePricPerKM[0]['pricePerKM']*number_format($distInKM,'2');
				$driverTripLoc['tripDropTime'] = time();
				$updateTripDetails = $this->Admin_model->updateTrip($tripId,$driverTripLoc);
				if(!empty($updateTripDetails)){
					$this->response([
						'responseMsg' => "trip completed status is updated",
						'responseCode' => 200,
						'responseData' => array(
							"finalTripAmount" => $driverTripLoc['finalTripAmount']
						),
					], REST_Controller::HTTP_BAD_REQUEST);
				}else{
					$this->response([
						'responseMsg' => "Invalid Params passed !",
						'responseCode' => 400,
					], REST_Controller::HTTP_BAD_REQUEST);
				}
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "Trip id is invalid or incorrect",
			], REST_Controller::HTTP_OK);
		}
	}

// ***************************************************************************************************************

// startTrip(D) - (travellerId,driverId,tripId,TripOTP,driverPickUp lat/long) [orderId in FCM]

	public function addAccountDetails_POST(){

		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);

		$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
			'responseMsg' => "userId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$bankName = (!empty($inputArray['bankName'])?$inputArray['bankName']:$this->response([
			'responseMsg' => "bankName is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$bankAccountNo = (!empty($inputArray['bankAccountNo'])?$inputArray['bankAccountNo']:$this->response([
			'responseMsg' => "bankAccountNo is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$bankIfsc = (!empty($inputArray['bankIfsc'])?$inputArray['bankIfsc']:$this->response([
			'responseMsg' => "bankIfsc is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$accHolderName = (!empty($inputArray['accHolderName'])?$inputArray['accHolderName']:$this->response([
			'responseMsg' => "accHolderName is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$userDetails['userHashId'] = $userId;
		$checkUserExist = $this->Admin_model->checkUserData($userDetails);

		if(!empty($checkUserExist)){
			$userBankDetails['bankName'] = $bankName;
			$userBankDetails['bankAccountNo'] = $bankAccountNo;
			$userBankDetails['bankIfsc'] = $bankIfsc;
			$userBankDetails['accHolderName'] = $accHolderName;
			$updateBankDetails = $this->Admin_model->updateBankDetails($userBankDetails,$userId);
			if(!empty($updateBankDetails)){
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "Bank Details were updated",
				], REST_Controller::HTTP_OK);
			}else{
				$this->response([
					'responseCode' => 400,
					'responseMsg' => "Bank Details were not updated! Try Again",
				], REST_Controller::HTTP_OK);
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "userId is invalid or incorrect",
			], REST_Controller::HTTP_OK);
		}
	}

// ****************************************************************************

	public function signOut_POST(){

		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);
		$userId = (!empty($inputArray['userId'])?$inputArray['userId']:$this->response([
			'responseMsg' => "userId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$userData['userHashId'] = $userId;
		$checkUserExist = $this->Admin_model->checkUserExist($userData);
		if(!empty($checkUserExist)){
			$deviceData['allowSignIn'] = 0;
			$updateDeviceDetails = $this->Admin_model->updateDeviceDetails($deviceData,$userId);
			if(!empty($updateDeviceDetails)){
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "sign out is enabled now, redirect to sign in page",
				], REST_Controller::HTTP_OK);
			}else{
				$this->response([
					'responseCode' => 400,
					'responseMsg' => "user not exist",
				], REST_Controller::HTTP_OK);
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "user not exist",
			], REST_Controller::HTTP_OK);
		}
	}

// *******************************************************************

	public function searchingForDriver_POST(){
		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);
		$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
			'responseMsg' => "travellerId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));
		$checkTripIsReqOrNot = $this->Admin_model->checkTripIsReqOrNot($travellerId);
		if(!empty($checkTripIsReqOrNot)){
			$checkAnyDriverConfirmedTrip = $this->Admin_model->checkAnyDriverConfirmedTrip($travellerId);
			if(!empty($checkAnyDriverConfirmedTrip)){
				$driverId = $checkAnyDriverConfirmedTrip[0]['driverId'];
				$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);
				$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($driverId);
				$TripDetails = array(
					"tripId" => $checkAnyDriverConfirmedTrip[0]['tripHashId'],
					"driverDetails"=> array(
						"driverName"=> $driverLatestLoc[0]['userName'],
						"driverEmail"=> $driverLatestLoc[0]['userEmail'],
						"driverMobileNo"=> $driverLatestLoc[0]['signInMobile'],
						"driverId"=> $driverLatestLoc[0]['userHashId'],
						"driverCurLat"=> $driverLatestLoc[0]['latitude'],
						"driverCurLong"=> $driverLatestLoc[0]['longitude']
					),
					"travellerDetails"=> array(
						"travellerName"=> $travellerLatestLoc[0]['userName'],
						"travellerEmail"=> $travellerLatestLoc[0]['userEmail'],
						"travellerMobileNo"=> $travellerLatestLoc[0]['signInMobile'],
						"travellerId"=> $travellerLatestLoc[0]['userHashId'],
						"travellerCurLat"=> $travellerLatestLoc[0]['latitude'],
						"travellerCurLong"=> $travellerLatestLoc[0]['longitude']
					),
				);
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "Trip acceptance Details",
					'responseData' => $TripDetails
				], REST_Controller::HTTP_OK);

			}else{
				$this->response([
					'responseCode' => 201,
					'responseMsg' => "Searching for driver confirming the trip still",
				], REST_Controller::HTTP_OK);
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "traveller have not requested for any trip or travellerId not found",
			], REST_Controller::HTTP_OK);
		}
	}

	

// ****************************************************************************************************

	public function driverReachingTime_POST(){

		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);

		$tripId = (!empty($inputArray['tripId'])?$inputArray['tripId']:$this->response([
			'responseMsg' => "tripId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST)); 

		$driverId = (!empty($inputArray['driverId'])?$inputArray['driverId']:$this->response([
			'responseMsg' => "driverId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
			'responseMsg' => "travellerId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$driverCurrLoc = $this->Admin_model->getDriverLatestLoc($driverId);
		$travellerCurrLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);

		if((!empty($driverCurrLoc))and(!empty($travellerCurrLoc))){
			$fromLatitude = $driverCurrLoc[0]['latitude'];
			$fromLongitude = $driverCurrLoc[0]['longitude'];
			$toLatitude = $travellerCurrLoc[0]['latitude'];
			$toLongitude = $travellerCurrLoc[0]['longitude'];
			$distInKM = $this->CalDis($fromLatitude,$fromLongitude,$toLatitude,$toLongitude,"K");
			if($distInKM<1){
				$this->response([
					'responseCode' => 400,
					'responseMsg' => "driver is reached your location",
				], REST_Controller::HTTP_OK);
			}else{
				$distInKM = number_format($distInKM,'2');
				$time = ceil(number_format(($distInKM/15),'2')*60);
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "driver is reaching your location",
					'responseData' => array("reachingTimeInMin"=>$time,"driverDetails"=>$driverCurrLoc),
				], REST_Controller::HTTP_OK);
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "driverId/travellerId is invalid",
			], REST_Controller::HTTP_OK);
		}
	}

// *********************************************************************************

	public function getTripDetails_POST(){
		$body = file_get_contents('php://input');
		$inputArray = json_decode(json_encode(json_decode($body)), true);
		$travellerId = (!empty($inputArray['travellerId'])?$inputArray['travellerId']:$this->response([
			'responseMsg' => "travellerId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$tripReqId = (!empty($inputArray['tripReqId'])?$inputArray['tripReqId']:$this->response([
			'responseMsg' => "tripReqId is required",
			'responseCode' => 400,
		], REST_Controller::HTTP_BAD_REQUEST));

		$checkTripIsReqOrNot = $this->Admin_model->checkTripIsReqOrNot($travellerId);

		if(!empty($checkTripIsReqOrNot)){
			$checkAnyDriverConfirmedTrip = $this->Admin_model->checkAnyDriverConfirmedTrip($travellerId);
			if(!empty($checkAnyDriverConfirmedTrip)){
				$driverId = $checkAnyDriverConfirmedTrip[0]['driverId'];
				$travellerLatestLoc = $this->Admin_model->getTravellerLatestLoc($travellerId);
				$driverLatestLoc = $this->Admin_model->getDriverLatestLoc($driverId);
				$journeyDetails = $this->Admin_model->getJourneyDetails($tripReqId);
				if(!empty($journeyDetails)){
					$vehicleId = $journeyDetails[0]['vehicleId']??null;
					$getvehicleTypeIs = $this->Admin_model->getVehicleConfig($vehicleId);
				}
				$vData['userHashId'] = $driverId;
				$checkVehicleData = $this->Admin_model->checkVehicleData($vData);

				$TripDetails = array(
					"tripId" => $checkAnyDriverConfirmedTrip[0]['tripHashId'],
					"driverDetails"=> array(
						"driverName"=> $driverLatestLoc[0]['userName']?? null,
						"driverEmail"=> $driverLatestLoc[0]['userEmail']?? null,
						"driverMobileNo"=> $driverLatestLoc[0]['signInMobile']?? null,
						"driverId"=> $driverLatestLoc[0]['userHashId']?? null,
						"driverCurLat"=> $driverLatestLoc[0]['latitude']?? null,
						"driverCurLong"=> $driverLatestLoc[0]['longitude']?? null
					),
					"travellerDetails"=> array(
						"travellerName"=> $travellerLatestLoc[0]['userName']??null,
						"travellerEmail"=> $travellerLatestLoc[0]['userEmail']??null,
						"travellerMobileNo"=> $travellerLatestLoc[0]['signInMobile']??null,
						"travellerId"=> $travellerLatestLoc[0]['userHashId']??null,
						"travellerCurLat"=> $travellerLatestLoc[0]['latitude']??null,
						"travellerCurLong"=> $travellerLatestLoc[0]['longitude']??null
					),
					"journeyDetails" => array(
						"toLat"=> $journeyDetails[0]['toLatitude']??null,   
						"toLong"=> $journeyDetails[0]['toLongitude']??null,
						"fronLat"=> $journeyDetails[0]['fromLatitude']??null,   
						"fromLong"=> $journeyDetails[0]['fromLongitude']??null,   
						"vehicleId"=> $journeyDetails[0]['vehicleId']??null,   
						"vehicleName"=> $getvehicleTypeIs[0]['vType']??null,  
						"vehicleNo"=> $checkVehicleData[0]['vehicleRegNo']??null   
					)
				);
				$this->response([
					'responseCode' => 200,
					'responseMsg' => "Trip acceptance Details",
					'responseData' => $TripDetails
				], REST_Controller::HTTP_OK);

			}else{
				$this->response([
					'responseCode' => 201,
					'responseMsg' => "drivers not yet accepted the trip",
				], REST_Controller::HTTP_OK);
			}
		}else{
			$this->response([
				'responseCode' => 400,
				'responseMsg' => "traveller have not requested for any trip or travellerId not found",
			], REST_Controller::HTTP_OK);
		}
	}

	// *****************************************************************************

}// end of class
?>