<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
		</div>  
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/vehiclePricingList' ?>">Vehicle Pricing List</a></div>
						<h2>
							<b>Add Vehicle and Pricing</b>
						</h2>
					</div>
					<div class="body">
						<form method="post" name="frmVehiclePricing" id="frmVehiclePricing" enctype="multipart/form-data">
							<label>Vehicle Name</label>
							<div class="form-group">
								<div class="form-line">
									<input type="text"  name="vehicleName"  id="vehicleName" class="form-control" placeholder="Enter Name" >
								</div>
							</div>

							<label>Vehicle Price Per KM</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="vehiclePricePerKM"  id="vehiclePricePerKM" class="form-control" placeholder="Enter Price Per KM" >
								</div>
							</div>

							<label>Vehicle Surcharges</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="vehicleSurcharges"  id="vehicleSurcharges" class="form-control" placeholder="Enter Price Per KM" >
								</div>
							</div>

							<label>GST - SGST</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="SGST"  id="SGST" class="form-control" placeholder="Enter GST - SGST" >
								</div>
							</div>

							<label>GST - CGST</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="CGST"  id="CGST" class="form-control" placeholder="Enter GST - CGST" >
								</div>
							</div>

							<label>Hourly Charges</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="hourlyCharge"  id="hourlyCharge" class="form-control" placeholder="Enter Hourly Charges" >
								</div>
							</div>

							<label>Base Fare</label>
							<div class="form-group">
								<div class="form-line">
									<input type="number"  name="baseFare"  id="baseFare" class="form-control" placeholder="Enter Base Fare" >
								</div>
							</div>

							<input type="submit" name="submit" class="btn btn-success m-t-15 waves-effect" value="Submit">
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<script>

</script>

