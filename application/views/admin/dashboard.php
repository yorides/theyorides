<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>DASHBOARD</h2>
		</div>
		<div class="row clearfix">
			<a>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-orange hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">person</i>
						</div>
						<div class="content">
							<div class="text" style="font-size:16px !important">Drivers</div>
							<div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20">
								<?php echo $driversCount; ?>
							</div>
						</div>
					</div>
				</div>
			</a>
			<a>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-blue hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">list</i>
						</div>
						<div class="content">
							<div class="text" style="font-size:16px !important">Travellers</div>
							<div class="number count-to" data-from="0" data-to="125" data-speed="2000" data-fresh-interval="20">
								<?php echo $travellerCount; ?>
							</div>
						</div>
					</div>
				</div>
			</a>

			<a>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-grey hover-zoom-effect">
						<div class="icon">
							<!-- <i class="material-icons">receipt</i> -->
							<i class="material-icons">message</i>
						</div>
						<div class="content">
							<div class="text" style="font-size:16px !important">Total OTP </div>
							<div class="number count-to" data-from="0" data-to="125" data-speed="2000" data-fresh-interval="20">
								<?php echo $otpCount; ?>
							</div>
						</div>
					</div>
				</div>
			</a>

			<a>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-brown hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">local_taxi</i>
						</div>
						<div class="content">
							<div class="text" style="font-size:16px !important">Vehicles Count</div>
							<div class="number count-to" data-from="0" data-to="125" data-speed="2000" data-fresh-interval="20">
								<?php echo $vehiclesCount; ?>
							</div>
						</div>
					</div>
				</div>
			</a>


		<!-- 	<a target="_blank" href="https://analytics.google.com/analytics/web/?authuser=1#/report-home/a167955626w234253886p219669317">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="info-box bg-pink hover-zoom-effect">
						<div class="icon">
							<i class="material-icons">done_all</i>
						</div>
						<div class="content">
							<div class="text" style="font-size:16px !important">Trips Completed</div>
							<div class="number count-to" data-from="0" data-to="125" data-speed="2000" data-fresh-interval="20">826</div>
						</div>
					</div>
				</div>
			</a> -->
		</div>
	</div>
</section>
