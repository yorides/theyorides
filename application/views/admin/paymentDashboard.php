		<script type="text/javascript">

			function delrecord(id){
				var yes = confirm("Are you Sure ?");
				if(yes){
					frmList.deleteid.value=id;
					frmList.submit();
					return true;
				}else{
					frmList.deleteid.value=null;
					return false;
				}
			}
		</script>
		<section class="content">
			<div class="container-fluid">
				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/addVehiclePricing' ?>">Add New Vehicle</a></div>
								<h2>
									<b>Vehicles and Pricing Details</b>
								</h2>
								<div class="text-center" style="padding-bottom:10px" id="err_hide">
									<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
									<span class="errStyle1"><?php echo $this->session->flashdata('DelSucc'); ?></span >
								</div>  
							</div>
							<form method="post" name="frmList" id="frmList">
								
								<div class="body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
											<thead>
												<tr>
													<th width="4%">Sl.No</th>
													<th width="10%">Vehicle</th>
													<th width="10%">Price/km</th>
													<th width="10%">Surcharge</th>
													<th width="5%">SGST</th>
													<th width="5%">CGST</th>
													<th width="10%">Base Fare</th>
													<th width="10%">Hourly Charges</th>
													<th width="10%">Action</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0; foreach ($dbVPData as $value) {
													$i++;	?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo ucwords($value['vType']); ?></td>
														<td><?php echo $value['pricePerKM']; ?></td>
														<td><?php echo $value['surCharge']; ?></td>
														<td><?php echo $value['SGST']; ?></td>
														<td><?php echo $value['CGST']; ?></td>
														<td><?php echo $value['baseFare']; ?></td>
														<td><?php echo $value['hourlyCharge']; ?></td>
														<td><input type="hidden" name="vehicleId" id="vehicleId" value="<?php echo $value['vehicleId'];?>">
															<a class="btn btn-xs btn-warning" title="Edit" href="<?php echo base_url().'admin/editVehiclePricing?vehicleId='.($value["vehicleId"]); ?>" target='_blank'> 
																Edit
															</a>
															&nbsp;
															&nbsp;
														</a>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- #END# Exportable Table -->
			</div>
		</section>
