<script type="text/javascript">
	function delrecord(id){
		var yes = confirm("Are you sure ?");
		if(yes){
			frmTaskList.deleteid.value = id;
			frmTaskList.submit();
		}else{
			frmTaskList.deleteid.value =null;
		}
	}
</script>	
<section class="content">
	<div class="container-fluid">
		<div class="text-center" style="padding-bottom:10px" id="err_hide">
			<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
			<span class="errStyle1"><?php echo $this->session->flashdata('DelSucc'); ?></span >
		</div> 
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<div id="delButtonPopUp" style="display:none;margin-right:60px" class="text-right">
							<div class="row clearfix js-sweetalert">
								<button class="btn btn-xs btn-danger" ><i class="material-icons" title="Delete Log" >delete_forever</i>Delete Selected  </button>
							</div>
						</div>
						<h2>
							<b>Travellers List</b>
						</h2>
					</div>
					<div class="body">
						<!-- <form name="frmTaskList" id="frmTaskList" method="post" > -->
							<?php $attr = array('name'=>'frmTaskList','method'=>'post','id'=>'frmTaskList'); 
							echo form_open('',$attr);
							?>
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
									<thead>
										<tr>
											<th width="1%">Sl.No</th>
											<th width="35%">Name</th>
											<th width="25%">Mobile</th>
											<th width="25%">Email</th>
											<th width="10%">Status</th>
											<th width="10%">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if(!empty($dbTravellerData)){
											$i =0;
											foreach ($dbTravellerData as $key => $value) {$i++
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo (!empty($value['userName']))?$value['userName']:"Not updated yet";?></td>
													<td><?php echo $value['signInMobile']; ?></td>
													<td><?php echo (!empty($value['userEmail']))?$value['userEmail']:"Not updated yet";?></td>
													<td>
														<?php if($value['allowSignIn']==1){ ?>
														<a style="font-size:10px !important" href="#" class="btn btn-success btn-xs">Active</a>
														<?php } elseif($value['allowSignIn']==0){ ?>
														<a style="font-size:10px !important" href="#" class="btn btn-danger btn-xs">Inactive</a>
														<?php } ?>
													</td>
													<td><input type="hidden" name="userHashId" id="userHashId" value="<?php echo $value['userHashId'];?>">
														<a class="btn btn-xs btn-warning" title="Edit" href="<?php echo base_url().'admin/editUser?userHashId='.($value["userHashId"]); ?>" target='_blank'> 
															Edit
														</a>
														&nbsp;
														&nbsp;
													</td>
													<?php } 
												} ?>
											</tbody>
										</table>
										<?php echo form_close(); ?>
										<!-- </form> -->
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- #END# Exportable Table -->
				</div>
			</section>
