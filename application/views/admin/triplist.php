		<script type="text/javascript">

			function delrecord(id){
				var yes = confirm("Are you Sure ?");
				if(yes){
					frmList.deleteid.value=id;
					frmList.submit();
					return true;
				}else{
					frmList.deleteid.value=null;
					return false;
				}
			}
		</script>
		<section class="content">
			<div class="container-fluid">
				<!-- Exportable Table -->
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<!-- 	<div class="align-right"><a type="button" class="btn btn-warning btn-sm" href="<?php echo base_url().'admin/addVehiclePricing' ?>">Add New Vehicle</a></div> -->
								<h2>
									<b>Trip Details</b>
								</h2>
								<div class="text-center" style="padding-bottom:10px" id="err_hide">
									<span class="errStyle"><?php echo $this->session->flashdata('Succ'); ?></span >
									<span class="errStyle1"><?php echo $this->session->flashdata('DelSucc'); ?></span >
								</div>  
							</div>
							<form method="post" name="frmList" id="frmList">
								
								<div class="body">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
											<thead>
												<tr style="font-size:10px !important">
													<th width="4%">Sl.No</th>
													<th width="10%">Traveller</th>
													<th width="10%">Driver</th>
													<th width="10%">VType</th>
													<th width="15%">Req Time</th>
													<th width="15%">From</th>
													<th width="15%">To </th>
													<th width="10%">PickUp</th>
													<th width="10%">Drop</th>
													<th width="10%">Status</th>
													<th width="10%">Amount</th>
												</tr>
											</thead>
											<tbody>
												<?php $i = 0; foreach ($dbTripData as $value) {
													// echo '<pre>';
													// print_r($value);
													// echo '</pre>';
													// break;

													$i++;	?>
													<tr style="font-size:10px !important">
														<td><?php echo $i; ?></td>
														<td><?php echo ucwords($value['travellerName']); ?></td>
														<td><?php echo $value['driverName']; ?></td>
														<td><?php echo $value['vehicleName']; ?></td>
														<td><?php echo date('d-m-y h:i A', $value['tripReqTime']);?></td>
														<td><?php echo !empty($value['fromAddress'])?$value['fromAddress']:"NA" ?></td>
														<td><?php echo !empty($value['toAddress'])?$value['toAddress']:"NA" ?></td>
														<td><?php echo !empty($value['tripPickUpTime'])?date('d-m-y h:i A', $value['tripPickUpTime']):"NA" ?></td>
														<td><?php echo !empty($value['tripDropTime'])?date('d-m-y h:i A', $value['tripDropTime']):"NA" ?></td>
														<td >
															<?php if($value['tripStatus']=='completed'){ ?>
															<a style="font-size:10px !important" href="#" class="btn btn-success btn-xs">Completed</a>
															<?php } elseif($value['tripStatus']=='cancelledByTraveller'){ ?>
															<a style="font-size:10px !important" href="#" class="btn btn-danger btn-xs">Canceled By Traveller</a>
															<?php } elseif($value['tripStatus']=='cancelledByDriver'){ ?>
															<a style="font-size:10px !important" href="#" class="btn btn-danger btn-xs">Canceled By Driver</a>
															<?php } elseif($value['tripStatus']=='started'){ ?>
															<a style="font-size:10px !important" href="#" class="btn btn-warning btn-xs">Started</a>
															<?php } elseif($value['tripStatus']=='accepted'){ ?>
															<a style="font-size:10px !important" href="#" class="btn btn-primary btn-xs">Accepted</a>
															<?php } elseif($value['tripStatus']=='requested'){ ?>
															<a style="font-size:10px !important" href="#" class="btn btn-default btn-xs">Requested</a>
															<?php } ?>
														</td>
														<td style="font-weight: bold !important"><?php echo  !empty($value['finalTripAmount'])?$value['finalTripAmount']:"NA" ?></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- #END# Exportable Table -->
				</div>
			</section>
